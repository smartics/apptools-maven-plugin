# Apptools Maven Plugin

## Overview

This [plugin](https://maven.apache.org/plugins/index.html)
supports the management of [doctype add-ons](https://www.smartics.eu/confluence/x/nQHJAw) ([OSGi Bundle Repository (OBR)](https://developer.atlassian.com/docs/faq/advanced-plugin-development-faq/bundling-extra-dependencies-in-an-obr)) and other apps for [Confluence](https://www.atlassian.com/software/confluence)
and the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/GQFk) with [Maven](https://maven.apache.org/).

More information is available on the [homepage of the Apptools Maven Plugin](https://www.smartics.eu/confluence/x/twF8Bg).

## Fork me!
Feel free to fork this project to meet your project requirements.

This plugin is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
