/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.apptools.domain;

import static org.junit.Assert.assertEquals;

import de.smartics.maven.apptools.domain.ArtifactSpecification;

import org.junit.Test;

/**
 *
 */
public class ArtifactSpecificationTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void supportsGA() {
    final String specString = "group:artifact";
    final ArtifactSpecification spec =
        ArtifactSpecification.fromString(specString);
    assertEquals(specString, spec.toString());
  }

  @Test
  public void supportsGAV() {
    final String specString = "group:artifact:version";
    final ArtifactSpecification spec =
        ArtifactSpecification.fromString(specString);
    assertEquals(specString, spec.toString());
  }

  @Test
  public void supportsGAEV() {
    final String specString = "group:artifact:ext:version";
    final ArtifactSpecification spec =
        ArtifactSpecification.fromString(specString);
    assertEquals(specString, spec.toString());
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsOnG() {
    final String specString = "group";
    ArtifactSpecification.fromString(specString);
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsOnClassifier() {
    final String specString = "group:artifact:ext:version:classifier";
    ArtifactSpecification.fromString(specString);
  }
}
