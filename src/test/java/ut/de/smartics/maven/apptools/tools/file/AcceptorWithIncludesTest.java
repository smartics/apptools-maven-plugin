/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.apptools.tools.file;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.smartics.maven.apptools.tools.file.Acceptor;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

/**
 * Tests {@link Acceptor}.
 */
public class AcceptorWithIncludesTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private Acceptor uut;

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  @Before
  public void setUp() {
    uut = Acceptor.create(Arrays.asList("A1", "A2"), null);
  }

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void supportsNotIncluded() {
    final File file = new File("N1");
    assertFalse(uut.isAccepted(file));
  }

  @Test
  public void supportsIncluded1NotRejected() {
    final File file = new File("A1");
    assertTrue(uut.isAccepted(file));
  }

  @Test
  public void supportsIncluded2NotRejected() {
    final File file = new File("A2");
    assertTrue(uut.isAccepted(file));
  }

  @Test
  public void supportsIncludedButRejectedFirst() {
    final File file = new File("A1R1");
    assertTrue(uut.isAccepted(file));
  }

  @Test
  public void supportsIncludedButRejectedSecond() {
    final File file = new File("A2R2");
    assertTrue(uut.isAccepted(file));
  }
}
