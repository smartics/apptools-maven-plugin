/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.apptools.tools.file;

import static org.junit.Assert.assertEquals;

import de.smartics.maven.apptools.tools.file.PluginKeyExtractor;
import de.smartics.maven.apptools.tools.file.PluginKeyExtractor.Metadata;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import test.de.smartics.maven.apptools.cp.FileTestUtils;

/**
 * Tests {@link PluginKeyExtractor}.
 */
public class PluginKeyExtractorTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private PluginKeyExtractor uut;

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  @Before
  public void setUp() {
    uut = new PluginKeyExtractor();
  }

  // --- helper ---------------------------------------------------------------

  @Test
  public void readsBuildPropertiesInJar() {
    final File file =
        FileTestUtils.getFileFromResource("jars/build-props/build-props.jar");
    final Metadata metadata = uut.fetchMetadata(file);
    final String pluginKey = metadata.getPluginKey();
    assertEquals("Cannot read from JAR's build.properties",
        "de.smartics.test.build.testId", pluginKey);
  }

  @Test
  public void readsManifestInJar() {
    final File file =
        FileTestUtils.getFileFromResource("jars/manifest/manifest.jar");
    final Metadata metadata = uut.fetchMetadata(file);
    final String pluginKey = metadata.getPluginKey();
    assertEquals("Cannot read from JAR's Manifest",
        "de.smartics.test.artifactId", pluginKey);
  }

  @Test
  public void readsPomInJar() {
    final File file =
        FileTestUtils.getFileFromResource("jars/pom-file/pom-file.jar");
    final Metadata metadata = uut.fetchMetadata(file);
    final String pluginKey = metadata.getPluginKey();
    assertEquals("Cannot read from JAR's pom.properties",
        "de.smartics.test.pom.testId", pluginKey);
  }

  @Test
  public void readsManifestInObr() {
    final File file =
        FileTestUtils.getFileFromResource("obrs/manifest/manifest.obr");
    final Metadata metadata = uut.fetchMetadata(file);
    final String pluginKey = metadata.getPluginKey();
    assertEquals("Cannot read from OBR's Manifest", "de.smartics.test.testId",
        pluginKey);
  }

  @Test
  public void readsJarInObr() {
    final File file = FileTestUtils.getFileFromResource("obrs/jar/jar.obr");
    final Metadata metadata = uut.fetchMetadata(file);
    final String pluginKey = metadata.getPluginKey();
    assertEquals("Cannot read from OBR's JAR/pom.properties",
        "de.smartics.test.pom.testId", pluginKey);
  }

  // --- tests ----------------------------------------------------------------

}
