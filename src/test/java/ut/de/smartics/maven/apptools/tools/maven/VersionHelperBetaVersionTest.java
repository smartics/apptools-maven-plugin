/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.apptools.tools.maven;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.smartics.maven.apptools.tools.maven.VersionHelper;

import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Test;

/**
 * Tests {@link VersionHelper#isBetaVersion(ArtifactVersion)}.
 */
public class VersionHelperBetaVersionTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void recognizesSnapshotBeta() {
    final ArtifactVersion version =
        new DefaultArtifactVersion("1.0.0.b4-SNAPSHOT");
    final boolean result = VersionHelper.isBetaVersion(version);
    assertTrue(result);
  }

  @Test
  public void recognizesBeta() {
    final ArtifactVersion version = new DefaultArtifactVersion("1.0.0.b3");
    final boolean result = VersionHelper.isBetaVersion(version);
    assertTrue(result);
  }

  @Test
  public void recognizesNotASnapshotBeta() {
    final ArtifactVersion version =
        new DefaultArtifactVersion("1.0.0-SNAPSHOT");
    final boolean result = VersionHelper.isBetaVersion(version);
    assertFalse(result);
  }

  @Test
  public void recognizesNotABeta() {
    final ArtifactVersion version = new DefaultArtifactVersion("1.0.0");
    final boolean result = VersionHelper.isBetaVersion(version);
    assertFalse(result);
  }

  @Test
  public void recognizesNotASnapshotBetaWithLeadingZero() {
    final ArtifactVersion version =
        new DefaultArtifactVersion("0.0.1-SNAPSHOT");
    final boolean result = VersionHelper.isBetaVersion(version);
    assertFalse(result);
  }

  @Test
  public void recognizesNotABetaWithLeadingZero() {
    final ArtifactVersion version = new DefaultArtifactVersion("0.0.1");
    final boolean result = VersionHelper.isBetaVersion(version);
    assertFalse(result);
  }
}
