/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.apptools.resources;

import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.resources.Plugins;
import de.smartics.maven.apptools.resources.Plugins.PluginMap;
import de.smartics.maven.apptools.tools.file.PluginKeyExtractor.Metadata;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests {@link Plugins}.
 */
public class PluginsTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String DEFAULT_QUALIFIER_DATE_PATTERN =
      "yyyyMMdd.HHmmss";

  private static final String PLUGIN_KEY = "de.smartics.test.plugin";

  private static final String PLUGIN_NAME = "smartics Test Plugin";

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  private static PluginMap createUut(final String version) {
    final Plugin plugin = createPlugin(version);
    final Plugins uut = Plugins.create(plugin);

    final PluginMap map = uut.getPluginMap(DEFAULT_QUALIFIER_DATE_PATTERN);
    return map;
  }

  private static Plugin createPlugin(final String version) {
    return Plugin.create(PLUGIN_KEY, PLUGIN_NAME, version);
  }

  private static SimpleDateFormat createDateFormat() {
    return new SimpleDateFormat(DEFAULT_QUALIFIER_DATE_PATTERN);
  }

  private static App createApp(final String appVersion,
      final String archiveDateString) {
    try {
      final DateFormat dateFormat = createDateFormat();
      final Date archiveDate = dateFormat.parse(archiveDateString);
      final Metadata metadata =
          Metadata.create(archiveDate, PLUGIN_KEY, appVersion);
      final App app = new App(null, dateFormat, metadata);
      return app;
    } catch (final ParseException e) {
      throw new IllegalStateException("Cannot parse archive date.", e);
    }
  }

  private static void isUpToDate(final String pluginVersion,
      final String appVersion, final String archiveDateString) {
    final boolean result =
        createResult(pluginVersion, appVersion, archiveDateString);
    assertTrue(result);
  }

  private static void isNotUpToDate(final String pluginVersion,
      final String appVersion, final String archiveDateString) {
    final boolean result =
        createResult(pluginVersion, appVersion, archiveDateString);
    assertFalse(result);
  }

  private static boolean createResult(final String pluginVersion,
      final String appVersion, final String archiveDateString) {
    final PluginMap map = createUut(pluginVersion);
    final App app = createApp(appVersion, archiveDateString);
    final boolean result = map.isUpToDate(app);
    return result;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void finalVersionPluginIsLater() {
    final String pluginVersion = "1.2.3";
    final String appVersion = "1.2.3-SNAPSHOT";
    final String archiveDateString = "20190301.143451";
    isUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void finalVersionAppIsLater() {
    final String pluginVersion = "1.2.3-SNAPSHOT";
    final String appVersion = "1.2.3";
    final String archiveDateString = "20190301.143451";

    isNotUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoEqualSnapshots() {
    final String pluginVersion = "1.2.3-20190301.143451";
    final String appVersion = "1.2.3-SNAPSHOT";
    final String archiveDateString = "20190301.143451";
    isUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoSnapshotsPluginIsLater() {
    final String pluginVersion = "1.2.4-20190322.143451";
    final String appVersion = "1.2.3-SNAPSHOT";
    final String archiveDateString = "20190301.143451";
    isUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoSnapshotsPluginIsLaterDate() {
    final String pluginVersion = "1.2.3-20190322.143451";
    final String appVersion = "1.2.3-SNAPSHOT";
    final String archiveDateString = "20190301.143451";
    isUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoSnapshotsAppIsLater() {
    final String pluginVersion = "1.2.3-20190201.143451";
    final String appVersion = "1.2.4-SNAPSHOT";
    final String archiveDateString = "20190301.143451";
    isNotUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoSnapshotsAppIsLaterDate() {
    final String pluginVersion = "1.2.3-20190201.143451";
    final String appVersion = "1.2.3-SNAPSHOT";
    final String archiveDateString = "20190301.143451";
    isNotUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoEqualFinals() {
    final String pluginVersion = "1.2.3";
    final String appVersion = "1.2.3";
    final String archiveDateString = "20190301.143451";
    isUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoFinalsPluginIsLater() {
    final String pluginVersion = "1.2.4";
    final String appVersion = "1.2.3";
    final String archiveDateString = "20190301.143451";
    isUpToDate(pluginVersion, appVersion, archiveDateString);
  }

  @Test
  public void twoFinalsAppIsLater() {
    final String pluginVersion = "1.2.3";
    final String appVersion = "1.2.4";
    final String archiveDateString = "20190301.143451";
    isNotUpToDate(pluginVersion, appVersion, archiveDateString);
  }
}
