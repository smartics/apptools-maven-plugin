/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.apptools.tools.maven;

import static org.junit.Assert.assertEquals;

import de.smartics.maven.apptools.tools.maven.VersionHelper;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.codehaus.mojo.versions.api.ArtifactVersions;
import org.codehaus.mojo.versions.ordering.MavenVersionComparator;
import org.junit.Test;

import java.util.Arrays;

/**
 * Tests {@link VersionHelper#getNewestUpdate(Artifact, ArtifactVersions)}.
 */
public class VersionHelperGetNewestUpdateTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final Artifact ARTIFACT = new DefaultArtifact("groupId",
      "artifactId", "version", "scope", "type", "classifier", null);

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  private static ArtifactVersions createVersions(
      ArtifactVersion[] artifactVersions) {
    return new ArtifactVersions(ARTIFACT, Arrays.asList(artifactVersions),
        new MavenVersionComparator());
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void withSnapshotBetas() throws OverConstrainedVersionException {
    final ArtifactVersions versions = createVersions(
        new ArtifactVersion[] {new DefaultArtifactVersion("1.0.0-SNAPSHOT"),
            new DefaultArtifactVersion("1.0.0.b1"),
            new DefaultArtifactVersion("1.0.0.b2-SNAPSHOT")});
    versions.setIncludeSnapshots(true);

    final ArtifactVersion version =
        VersionHelper.getNewestUpdate(ARTIFACT, versions);
    assertEquals("1.0.0-SNAPSHOT", version.toString());
  }

  @Test
  public void withBetas() throws OverConstrainedVersionException {
    final ArtifactVersions versions = createVersions(
        new ArtifactVersion[] {new DefaultArtifactVersion("1.0.0"),
            new DefaultArtifactVersion("1.0.0.b1"),
            new DefaultArtifactVersion("1.0.0.b2")});

    final ArtifactVersion version =
        VersionHelper.getNewestUpdate(ARTIFACT, versions);
    assertEquals("1.0.0", version.toString());
  }
}
