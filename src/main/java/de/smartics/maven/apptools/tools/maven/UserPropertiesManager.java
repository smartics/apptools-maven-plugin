/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.maven;

import de.smartics.maven.apptools.tools.file.StringUtilities;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.execution.MavenSession;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Provides convenient access to properties set by the user, typically on the
 * commandline using <tt>-Dname=value</tt>.
 */
public class UserPropertiesManager {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final MavenSession session;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public UserPropertiesManager(final MavenSession session) {
    this.session = session;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public boolean getPropertyAsBoolean(final String group, final String name) {
    return getPropertyAsBoolean(group + '.' + name);
  }

  public boolean getPropertyAsBoolean(final String fullPropertyName) {
    final String value = getProperty(fullPropertyName);
    return "true".equals(value);
  }

  public String getProperty(final String group, final String name) {
    return getProperty(group + '.' + name);
  }

  public String getProperty(final String fullPropertyName) {
    final Properties properties = session.getUserProperties();
    if (properties != null) {
      final String value = properties.getProperty(fullPropertyName);
      return value;
    }
    return null;
  }

  public List<String> getPropertyAsList(final String fullPropertyName) {
    final String value = getProperty(fullPropertyName);
    if (value != null) {
      final List<String> list = StringUtilities.splitByCommaToList(value);
      return list;
    }
    return new ArrayList<>(1);
  }

  public File getPropertyAsFile(final File baseDir,
      final String fullPropertyName) {
    final String value = getProperty(fullPropertyName);
    if (value != null) {
      final File file = new File(value);
      if (file.isAbsolute()) {
        return file;
      } else {
        return new File(baseDir, value);
      }
    }
    return null;
  }

  public Integer getPropertyAsInteger(final String fullPropertyName) {
    final String value = getProperty(fullPropertyName);
    if (StringUtils.isNotBlank(value)) {
      try {
        final int intValue = Integer.parseInt(value);
        return intValue;
      } catch (final NumberFormatException e) {
        // return null;
      }
    }
    return null;
  }

  public Long getPropertyAsLong(final String fullPropertyName) {
    final String value = getProperty(fullPropertyName);
    if (StringUtils.isNotBlank(value)) {
      try {
        final long longValue = Long.parseLong(value);
        return longValue;
      } catch (final NumberFormatException e) {
        // return null;
      }
    }
    return null;
  }

  // --- object basics --------------------------------------------------------

}
