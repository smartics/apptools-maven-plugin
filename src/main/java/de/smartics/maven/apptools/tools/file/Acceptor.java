/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.file;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper to filter by string includes and excludes.
 */
public final class Acceptor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private final Pattern FILENAME = Pattern.compile("^(.+?)-(\\d.*?)\\.\\S{3}$");

  // --- members --------------------------------------------------------------

  private final List<String> includes;

  private final List<String> excludes;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private Acceptor(final List<String> includes, final List<String> excludes) {
    this.includes = includes;
    this.excludes = excludes;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static Acceptor create(final List<String> includes,
      final List<String> excludes) {
    return new Acceptor(includes, excludes);
  }

  public static Acceptor create(final String include) {
    final List<String> includes = Collections.singletonList(include);
    final List<String> excludes = null;
    return create(includes, excludes);
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public boolean isAccepted(final File file) {
    final String name = file.getName();
    return isIncluded(name);
  }

  public boolean isAccepted(final String name) {
    return isIncluded(name);
  }

  private boolean isIncluded(final String filename) {
    final String fileNameWithoutVersionAndExtension = parseFilename(filename);
    if (includes != null && !includes.isEmpty()) {
      for (final String fragment : includes) {
        if (null == fragment) {
          // if user sets a comma at the end, the last value is 'null'
          continue;
        }

        if (isSuffixMatch(fragment)) {
          if (fileNameWithoutVersionAndExtension
              .endsWith(calcSuffix(fragment))) {
            return !isExcluded(fileNameWithoutVersionAndExtension);
          }
        } else {
          if (fileNameWithoutVersionAndExtension.contains(fragment)) {
            return !isExcluded(fileNameWithoutVersionAndExtension);
          }
        }
      }
      return false;
    }
    return !isExcluded(filename);
  }

  private String parseFilename(String completeFilename) {
    final Matcher matcher = FILENAME.matcher(completeFilename);
    if (matcher.find()) {
      return matcher.group(1);
    }
    return completeFilename;
  }

  private String calcSuffix(final String fragment) {
    return fragment.substring(0, fragment.length() - 1);
  }

  private boolean isSuffixMatch(final String fragment) {
    return fragment.endsWith("$") && fragment.length() > 1;
  }

  private boolean isExcluded(final String filename) {
    if (excludes != null && !excludes.isEmpty()) {
      for (final String fragment : excludes) {
        if (null == fragment) {
          // if user sets a comma at the end, the last value is 'null'
          continue;
        }
        if (isSuffixMatch(fragment)) {
          if (filename.endsWith(calcSuffix(fragment))) {
            return true;
          }
        } else {
          if (filename.contains(fragment)) {
            return true;
          }
        }
      }
    }
    return false;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "includes=" + includes + ", excludes=" + excludes;
  }
}
