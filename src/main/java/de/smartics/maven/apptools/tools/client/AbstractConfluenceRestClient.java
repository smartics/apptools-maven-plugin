/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.client;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.ExecutionContext;
import de.smartics.maven.apptools.domain.RemoteAppException;
import de.smartics.maven.apptools.tools.enable.ConfluenceRestEnablementChecker;
import de.smartics.maven.apptools.tools.file.StringUtilities;
import de.smartics.maven.apptools.tools.maven.ArtifactDownloadHelper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.codehaus.plexus.util.StringUtils;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.Base64;

/**
 * Check which apps are installed on a remote Confluence server via a REST API.
 */
public abstract class AbstractConfluenceRestClient<T extends AbstractConfluenceRestClient<T>>
    implements Closeable {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  protected final ExecutionContext context;

  protected final CloseableHttpClient client;

  protected final String username;

  protected final boolean hasPassword;

  protected final BasicHeader authHeader;

  private final String tokenServiceUrl;

  protected final String restServiceUrl;

  protected final ConfluenceRestEnablementChecker checker;

  protected final DateFormat qualifierDateFormat;

  protected final ArtifactDownloadHelper artifactDownloadHelper;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected AbstractConfluenceRestClient(final Builder<T> builder) {
    this.context = builder.context;
    this.client = builder.client;
    this.username = builder.username;
    this.hasPassword = StringUtils.isNotBlank(builder.password);
    this.authHeader = builder.authHeader;
    this.tokenServiceUrl = builder.serverUrl + builder.tokenServicePath;
    this.restServiceUrl = builder.serverUrl + builder.servicePath;
    this.checker = builder.checker;
    this.qualifierDateFormat = builder.qualifierDateFormat;
    this.artifactDownloadHelper = builder.artifactDownloadHelper;
  }

  // ****************************** Inner Classes *****************************

  public static abstract class Builder<T> {
    protected final ExecutionContext context;

    protected int timeoutMs;

    protected String username;

    protected String password;

    protected String serverUrl;

    private String tokenServicePath;

    protected String servicePath;

    protected CloseableHttpClient client;

    protected BasicHeader authHeader;

    protected ConfluenceRestEnablementChecker checker;

    protected DateFormat qualifierDateFormat;

    protected ArtifactDownloadHelper artifactDownloadHelper;

    protected Builder(final ExecutionContext context) {
      this.context = context;
    }

    public Builder<T> timeoutMs(final int timeoutMs) {
      this.timeoutMs = timeoutMs;
      return this;
    }

    public Builder<T> username(final String username) {
      this.username = username;
      return this;
    }

    public Builder<T> password(final String password) {
      this.password = password;
      return this;
    }

    public Builder<T> serverUrl(final String serverUrl) {
      this.serverUrl = StringUtilities.chopPathDelimiter(serverUrl);
      return this;
    }

    public Builder<T> tokenServicePath(final String tokenServicePath) {
      this.tokenServicePath =
          StringUtilities.padPathDelimiter(tokenServicePath);
      return this;
    }

    public Builder<T> servicePath(final String servicePath) {
      this.servicePath = StringUtilities.padPathDelimiter(servicePath);
      return this;
    }

    public Builder<T> checker(final ConfluenceRestEnablementChecker checker) {
      this.checker = checker;
      return this;
    }

    @SuppressFBWarnings("EI_EXPOSE_REP2")
    public Builder<T> qualifierDateFormat(
        final DateFormat qualifierDateFormat) {
      this.qualifierDateFormat = qualifierDateFormat;
      return this;
    }

    public Builder<T> artifactDownloadHelper(
        final ArtifactDownloadHelper artifactDownloadHelper) {
      this.artifactDownloadHelper = artifactDownloadHelper;
      return this;
    }


    public abstract T build();

    protected void init() {
      client = createHttpClient(timeoutMs, username, password);
      authHeader = createAuthHeader();
    }

    protected static CloseableHttpClient createHttpClient(final int timeoutMs,
        final String username, final String password) {
      final HttpClientBuilder httpClientBuilder = HttpClients.custom()
          .setDefaultRequestConfig(RequestConfig.custom()
              .setConnectionRequestTimeout(timeoutMs)
              .setConnectTimeout(timeoutMs)
              .setSocketTimeout(timeoutMs)
              .build());
      final CredentialsProvider credentialsProvider =
          new BasicCredentialsProvider();
      credentialsProvider.setCredentials(AuthScope.ANY,
          new UsernamePasswordCredentials(username, password));
      httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);

      return httpClientBuilder.build();
    }

    protected BasicHeader createAuthHeader() {
      return new BasicHeader("authorization", "Basic " + Base64.getEncoder()
          .encodeToString(
              (username + ':' + password).getBytes(StandardCharsets.UTF_8)));
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  protected String fetchToken() throws RemoteAppException {
    context.logVerbose("  Fetching token using service ...");
    final HttpHead request = createTokenRequest();

    context.logVerbose("    submitting request " + request + " ...");
    context.logVerbose("      with username '" + username + "' and password is "
        + (hasPassword ? "" : "not ") + "set ...");

    try (CloseableHttpResponse response = client.execute(request)) {
      final StatusLine statusLine = response.getStatusLine();
      if (statusLine != null) {
        final int statusCode = statusLine.getStatusCode();
        if (statusCode >= 400) {
          throw new RemoteAppException("Cannot fetch token from '"
              + tokenServiceUrl + "' since server responded with code "
              + statusCode + ": " + statusLine.getReasonPhrase(), statusCode);
        }
      }

      final Header[] headers = response.getHeaders("upm-token");
      if (headers.length == 0) {
        throw new AppException(
            "Cannot fetch token from UPM since header has no 'upm-token': "
                + tokenServiceUrl);
      }
      final String token = headers[0].getValue();
      context.logVerbose("  Token fetched successfully.");
      return token;
    } catch (final Exception e) {
      throw new AppException(
          "Cannot execute request to remote REST service '" + tokenServiceUrl
              + "' to fetch authentication token: " + e.getMessage(),
          e);
    }
  }

  private HttpHead createTokenRequest() throws AppException {
    try {
      final HttpHead request = new HttpHead();
      request.setURI(new URI(tokenServiceUrl + "?os_authType=basic"));
      request.setHeader(authHeader);
      request.setHeader("Accept", "application/vnd.atl.plugins.installed+json");
      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to access remote REST service: "
              + e.getMessage(),
          e);
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public void close() throws IOException {
    client.close();
  }
}
