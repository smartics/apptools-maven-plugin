/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.file;

import org.codehaus.plexus.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Provides utility functions on strings.
 */
public class StringUtilities {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final Pattern SPLIT_PATTERN = Pattern.compile("\\s*,\\s*");

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static String chopPathDelimiter(final String path) {
    if (path == null) {
      return org.apache.commons.lang3.StringUtils.EMPTY;
    }
    final int pathLength = path.length();
    if (pathLength == 0) {
      return org.apache.commons.lang3.StringUtils.EMPTY;
    }

    if (endsWithSlash(path)) {
      return path.substring(0, pathLength - 2);
    }
    return path;
  }

  public static String padPathDelimiter(final String path)
      throws NullPointerException {
    if (path == null) {
      return org.apache.commons.lang3.StringUtils.EMPTY;
    }
    final int pathLength = path.length();
    if (pathLength == 0) {
      return org.apache.commons.lang3.StringUtils.EMPTY;
    }

    final StringBuilder newPath = new StringBuilder(pathLength + 2);
    if (startsWithSlash(path)) {
      newPath.append('/');
    }
    newPath.append(path);
    if (!endsWithSlash(path)) {
      newPath.append('/');
    }

    return newPath.toString();
  }

  private static boolean startsWithSlash(final String path) {
    return path.charAt(0) != '/';
  }

  private static boolean endsWithSlash(final String path) {
    return path.charAt(path.length() - 1) == '/';
  }

  public static boolean hasNoLowerCase(final String input) {
    if (StringUtils.isBlank(input)) {
      return false;
    }
    for (int i = input.length() - 1; i >= 0; i--) {
      final char ch = input.charAt(i);
      if (Character.isLowerCase(ch)) {
        return false;
      }
    }
    return true;
  }

  public static List<String> splitByCommaToList(final String value) {
    if (value == null) {
      return new ArrayList<>(0);
    }
    final ArrayList<String> list = new ArrayList<>(value.length());
    for (final String element : splitByComma(value, true)) {
      list.add(element);
    }
    return list;
  }

  public static String[] splitByComma(final String value, final boolean trim) {
    return splitBy(value, SPLIT_PATTERN, trim);
  }

  public static String[] splitBy(final String value,
      final Pattern splitPattern) {
    return splitBy(value, splitPattern, true);
  }

  public static String[] splitBy(final String value, final Pattern splitPattern,
      final boolean trim) {
    if (StringUtils.isBlank(value)) {
      return new String[0];
    }

    final String[] tokens = splitPattern.split(value);
    if (trim) {
      for (int i = tokens.length - 1; i >= 0; i--) {
        final String normalizedToken = tokens[i].trim();
        tokens[i] = normalizedToken;
      }
    }
    return tokens;
  }


  // --- object basics --------------------------------------------------------

}
