/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.deploy;

import com.google.gson.Gson;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.mojo.AbstractArtifactMojo.BaseArtifactContext;
import de.smartics.maven.apptools.resources.PluginMetadata;
import de.smartics.maven.apptools.tools.client.AbstractConfluenceRestClient;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

/**
 * Fetches matadata for a single apps from a remote Confluence server via a REST
 * API.
 *
 * @see <a href= "https://ecosystem.atlassian
 * .net/wiki/spaces/UPM/pages/6094960/UPM+REST+API">UPM REST API</a>
 */
public class ConfluenceRestAppMetadata
    extends AbstractConfluenceRestClient<ConfluenceRestAppMetadata> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ConfluenceRestAppMetadata(
      final Builder<ConfluenceRestAppMetadata> builder) {
    super(builder);
  }

  // ****************************** Inner Classes *****************************


  public static final class MetadataFetcherBuilder
      extends Builder<ConfluenceRestAppMetadata> {
    public MetadataFetcherBuilder(final BaseArtifactContext context) {
      super(context);
      servicePath("/rest/plugins/1.0/available/${plugin.key}-key");
    }

    public ConfluenceRestAppMetadata build() {
      init();
      return new ConfluenceRestAppMetadata(this);

    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static MetadataFetcherBuilder a(final BaseArtifactContext context) {
    return new MetadataFetcherBuilder(context);
  }

  // --- get&set --------------------------------------------------------------

  //  private Context getContext() {
  //    return (Context) context;
  //  }

  // --- business -------------------------------------------------------------

  public PluginMetadata metadata(final String pluginKey) throws AppException {
    final String restServiceUrl =
        this.restServiceUrl.replace("${plugin.key}", pluginKey);
    final String token = fetchToken();
    return metadata(restServiceUrl, token, pluginKey);
  }

  private PluginMetadata metadata(final String restServiceUrl,
      final String token, final String pluginKey) throws AppException {
    context.logVerbose(
        "  Fetching metadata for app '" + pluginKey + "' from '" +
        restServiceUrl + "' ...");
    final HttpGet request = createCheckRequest(restServiceUrl, token);

    try (final CloseableHttpResponse response = client.execute(request)) {
      final int statusCode = response.getStatusLine().getStatusCode();
      if (statusCode != 200) {
        if (statusCode == 404) {
          return null;
        } else {
          throw new AppException(
              "Cannot fetch metadata about deployed app '" + pluginKey +
              "' from remote '" + restServiceUrl + "': " +
              response.getStatusLine());
        }
      }

      final HttpEntity entity = response.getEntity();

      try (final Reader in = new InputStreamReader(
          new BufferedInputStream(entity.getContent()),
          StandardCharsets.UTF_8)) {
        final Gson gson = new Gson();
        return gson.fromJson(in, PluginMetadata.class);
      }
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException(
          "Cannot fetch metadata for app '" + pluginKey + "' via '" +
          restServiceUrl + "'.", e);
    }
  }

  private HttpGet createCheckRequest(final String restServiceUrl,
      final String token) throws AppException {
    try {
      final HttpGet request = new HttpGet();
      request.setHeader(authHeader);
      request.setHeader("Accept", "application/vnd.atl.plugins.available+json");
      request.setURI(new URI(restServiceUrl + "?token=" + token));
      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to request app metadata installed on remote " +
          "server via '" + restServiceUrl + "'.", e);
    }
  }
  // --- object basics --------------------------------------------------------

}
