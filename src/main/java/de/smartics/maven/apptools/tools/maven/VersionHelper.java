/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.maven;

import de.smartics.maven.apptools.domain.App;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.codehaus.mojo.versions.api.ArtifactVersions;
import org.codehaus.mojo.versions.api.UpdateScope;
import org.codehaus.plexus.util.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Helper to create an App where the qualifier SNAPSHOT is replaced with the
 * creation date of the app's archive. This replacement is skipped if the plugin
 * does not provide a qualifier of the required date format.
 */
public class VersionHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String qualifierDatePattern;

  private final App app;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public VersionHelper(final String qualifierDatePattern, final App app) {
    this.qualifierDatePattern = qualifierDatePattern;
    this.app = app;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public ArtifactVersion createVersion(final ArtifactVersion pluginVersion,
      final String appVersionString) {
    final String qualifier = pluginVersion.getQualifier();
    if (StringUtils.isNotBlank(qualifier) && !"SNAPSHOT".equals(qualifier) &&
        appVersionString.endsWith("-SNAPSHOT")) {
      final DateFormat format = new SimpleDateFormat(qualifierDatePattern);
      try {
        format.parse(qualifier);
        final Date appDate = app.getCreationDate();
        final String appDateString = format.format(appDate);
        final String appVersionStringWithDate =
            appVersionString.substring(0, appVersionString.length() - 8) +
            appDateString;
        return new DefaultArtifactVersion(appVersionStringWithDate);
      } catch (final ParseException e) {
        // okay, qualifier is not of the expected format, therefore we do not
        // replace "SNAPSHOT" with the formatted date.
      }
    }
    return new DefaultArtifactVersion(appVersionString);
  }

  public boolean isQualified(final ArtifactVersion pluginVersion) {
    final String qualifier = pluginVersion.getQualifier();
    return (StringUtils.isNotBlank(qualifier) && !"SNAPSHOT".equals(qualifier));
  }

  public static String fetchTimestampPart(final String value) {
    final int index = value.indexOf('-');
    if (index > 0 && index < value.length() - 2) {
      return value.substring(index + 1);
    }

    return null;
  }

  public static int compareNonQualified(final ArtifactVersion version1,
      final ArtifactVersion version2) {
    int result = version1.getMajorVersion() - version2.getMajorVersion();
    if (result == 0) {
      result = version1.getMinorVersion() - version2.getMinorVersion();
      if (result == 0) {
        result =
            version1.getIncrementalVersion() - version2.getIncrementalVersion();
      }
    }
    return result;
  }

  /**
   * Helper that understands beta versions of the form x.x.x.beta-SNAPSHOT and
   * x.x.x.beta.
   */
  public static ArtifactVersion getNewestUpdate(final Artifact artifact,
      final ArtifactVersions versions) throws OverConstrainedVersionException {
    final ArtifactVersion artifactVersion = artifact.getSelectedVersion();
    final ArtifactVersion officialNewest =
        versions.getNewestUpdate(artifactVersion, UpdateScope.ANY);
    if (officialNewest == null || !isBetaVersion(officialNewest)) {
      return officialNewest;
    }

    final ArtifactVersion latestNonBeta = calculateNonBeta(officialNewest);
    final ArtifactVersion[] nonBetaVersions =
        versions.getVersions(latestNonBeta, latestNonBeta,
            versions.isIncludeSnapshots(), true, true);
    if (nonBetaVersions != null && nonBetaVersions.length > 0) {
      final ArtifactVersion selected = nonBetaVersions[0];
      if (artifactVersion.compareTo(selected) == 0) {
        return null;
      }
      return selected;
    }
    return officialNewest;
  }

  /**
   * It is expected that the provided version is a beta version, where the beta
   * identifier part is separated from the micro version by a dot.
   */
  public static ArtifactVersion calculateNonBeta(
      final ArtifactVersion betaVersion) {
    final String versionString = betaVersion.toString();
    final int qualifierIndex = versionString.indexOf('-');
    if (qualifierIndex >= 0) {
      final int betaIndex = versionString.lastIndexOf('.', qualifierIndex);
      return new DefaultArtifactVersion(versionString.substring(0, betaIndex) +
                                        versionString.substring(
                                            qualifierIndex));
    } else {
      final int betaIndex = versionString.lastIndexOf('.');
      return new DefaultArtifactVersion(versionString.substring(0, betaIndex));
    }
  }

  public static boolean isBetaVersion(final ArtifactVersion version) {
    final String qualifier = version.getQualifier();
    return (version.getMajorVersion() == 0 &&
            version.toString().equals(qualifier)) ||
           (qualifier != null && !"SNAPSHOT".equals(qualifier));
  }

  // --- object basics --------------------------------------------------------

}
