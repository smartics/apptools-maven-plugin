/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.deploy;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.RemoteAppException;
import de.smartics.maven.apptools.domain.UndeployTarget;
import de.smartics.maven.apptools.mojo.UndeployMojo.Context;
import de.smartics.maven.apptools.tools.client.AbstractConfluenceRestClient;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Undeploys apps from a remote Confluence server via a REST API.
 *
 * @see <a href=
 *      "https://ecosystem.atlassian.net/wiki/spaces/UPM/pages/6094960/UPM+REST+API">UPM
 *      REST API</a>
 */
public class ConfluenceRestUndeployer
    extends AbstractConfluenceRestClient<ConfluenceRestUndeployer>
    implements UndeployTarget {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ConfluenceRestUndeployer(
      final Builder<ConfluenceRestUndeployer> builder) {
    super(builder);
  }

  // ****************************** Inner Classes *****************************

  public static final class UndeployerBuilder
      extends Builder<ConfluenceRestUndeployer> {
    public UndeployerBuilder(final Context context) {
      super(context);
      servicePath("/rest/plugins/1.0/${plugin.key}-key");
    }

    public ConfluenceRestUndeployer build() {
      init();
      return new ConfluenceRestUndeployer(this);

    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static UndeployerBuilder a(final Context context) {
    return new UndeployerBuilder(context);
  }

  // --- get&set --------------------------------------------------------------

//  private Context getContext() {
//    return (Context) context;
//  }

  // --- business -------------------------------------------------------------

  public void undeploy(final String pluginKey) throws RemoteAppException {
    final String restServiceUrl =
        this.restServiceUrl.replace("${plugin.key}", pluginKey);
    if (!context.isDryRun()) {
      final String token = fetchToken();
      undeployApp(restServiceUrl, token, pluginKey);
    } else {
      context.logInfo("If not in dryRun mode, app '" + pluginKey
          + "' would be undeployed from '" + restServiceUrl + "'.");
    }
  }

  private void undeployApp(final String restServiceUrl, final String token,
      final String pluginKey) throws RemoteAppException {
    context.logVerbose("  Undeploying app '" + pluginKey + "' ...");
    final HttpDelete request =
        createUndeployRequest(restServiceUrl, token, pluginKey);

    try (final CloseableHttpResponse response = client.execute(request)) {
      final int statusCode = response.getStatusLine()
          .getStatusCode();
      if (statusCode != 204) {
        throw new RemoteAppException("Cannot undeploy app " + pluginKey + ": "
            + response.getStatusLine(), statusCode);
      }
      context.logInfo("  Undeployment request for app '" + pluginKey
          + "' successfully accepted by '" + restServiceUrl + "'.");
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException(
          "Cannot undeploy app " + pluginKey + " to REST service.", e);
    }
  }

  private HttpDelete createUndeployRequest(final String restServiceUrl,
      final String token, final String pluginKey) throws AppException {
    try {
      final HttpDelete request = new HttpDelete();
      request.setHeader(authHeader);
      request.setURI(new URI(restServiceUrl + "?token=" + token));
      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to post app to REST service.", e);
    }
  }

  // --- object basics --------------------------------------------------------

}
