/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.enable;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.RemoteAppException;
import de.smartics.maven.apptools.mojo.DeployMojo.Context;
import de.smartics.maven.apptools.tools.client.AbstractConfluenceRestClient;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Checks the enablement status of apps on a remote Confluence server via a REST
 * API.
 */
public class ConfluenceRestEnablementChecker
    extends AbstractConfluenceRestClient<ConfluenceRestEnablementChecker> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ConfluenceRestEnablementChecker(
      final Builder<ConfluenceRestEnablementChecker> builder) {
    super(builder);
  }

  // ****************************** Inner Classes *****************************


  public static final class CheckerBuilder
      extends Builder<ConfluenceRestEnablementChecker> {
    public CheckerBuilder(final Context context) {
      super(context);
      servicePath("/rest/plugins/1.0/${plugin.key}-key/summary");
    }

    public ConfluenceRestEnablementChecker build() {
      init();
      return new ConfluenceRestEnablementChecker(this);

    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static CheckerBuilder a(final Context context) {
    return new CheckerBuilder(context);
  }

  // --- get&set --------------------------------------------------------------

  //  private Context getContext() {
  //    return (Context) context;
  //  }

  // --- business -------------------------------------------------------------

  public boolean isInState(final PluginSummary plugin)
      throws RemoteAppException {
    final String token = fetchToken();
    return checkApp(token, plugin);
  }


  private boolean checkApp(final String token,
      final PluginSummary expectedPlugin) throws RemoteAppException {
    context.logVerbose(
        "  Checking enablement state of " + expectedPlugin + " ...");

    final String url =
        restServiceUrl.replace("${plugin.key}", expectedPlugin.getKey());
    final HttpGet request = createRequest(url, token);
    try (final CloseableHttpResponse response = client.execute(request)) {
      final int statusCode = response.getStatusLine().getStatusCode();
      if (statusCode != 200) {
        if (statusCode == 404) {
          return false;
        }
        throw new RemoteAppException(
            "Cannot enable apps: " + response.getStatusLine(), statusCode);
      }
      context.logInfo(
          "  Enablement request for apps has been successfully accepted by '" +
          url + "'.");

      final HttpEntity entity = response.getEntity();

      final PluginSummary pluginSummary;
      final Gson gson = new Gson();
      if (context.isDebug()) {
        final String returnedJson =
            IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8);
        pluginSummary = gson.fromJson(returnedJson, PluginSummary.class);
        context.logDebug("Returned JSON:\n" + returnedJson);
      } else {
        pluginSummary = gson.fromJson(new JsonReader(
                new InputStreamReader(entity.getContent(),
                    StandardCharsets.UTF_8)),
            PluginSummary.class);
      }
      return isReportedEnabled(expectedPlugin, pluginSummary);
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException(
          "Cannot check enablement state of app " + expectedPlugin +
          " on remote server.", e);
    }
  }

  private HttpGet createRequest(final String url, final String token)
      throws AppException {
    try {
      final HttpGet request = new HttpGet();
      request.setHeader(authHeader);
      request.setURI(new URI(url + "?token=" + token));
      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to check enablement status of app via REST " +
          "service.", e);
    }
  }

  private boolean isReportedEnabled(final PluginSummary expectedPlugin,
      final PluginSummary pluginSummary) {
    if (pluginSummary != null) {
      final boolean matchesEnablementState =
          (expectedPlugin.isEnabled() == pluginSummary.isEnabled());
      final String expectedVersion = expectedPlugin.getVersion();
      final String reportedVersion = pluginSummary.getVersion();
      return matchesEnablementState && (expectedVersion == null ||
                                        Objects.equals(expectedVersion,
                                            reportedVersion));
    }

    return false;
  }

  // --- object basics --------------------------------------------------------

}
