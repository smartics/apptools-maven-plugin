/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.enable;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class EnablementPlugin {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private String key;

  private List<Module> modules = new ArrayList<>();

  private boolean enabled;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public EnablementPlugin(final String key, final boolean enabled) {
    this.key = key;
    this.enabled = enabled;
  }

  public EnablementPlugin() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getKey() {
    return key;
  }

  @SuppressFBWarnings("EI_EXPOSE_REP")
  public List<Module> getModules() {
    if (modules == null) {
      modules = new ArrayList<>();
    }
    return modules;
  }

  public boolean isEnabled() {
    return enabled;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "enabled=" + enabled + " > " + key;
  }
}
