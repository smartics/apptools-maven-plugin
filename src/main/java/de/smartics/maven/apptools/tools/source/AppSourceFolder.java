/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.source;

import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppSource;
import de.smartics.maven.apptools.domain.FileAcceptorContext;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Handles application files found in a given folder.
 */
public class AppSourceFolder implements AppSource {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final FilenameFilter APP_FILENAME_FILTER =
      new FilenameFilter() {
        @Override
        public boolean accept(final File dir, final String name) {
          return (name.endsWith(".obr") || name.endsWith(".jar"));
        }
      };

  // --- members --------------------------------------------------------------

  private final FileAcceptorContext context;

  private final File folder;

  private final File[] appFiles;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private AppSourceFolder(final FileAcceptorContext context, final File folder,
      final File[] appFiles) {
    this.context = context;
    this.folder = folder;
    this.appFiles = appFiles;
  }

  // ****************************** Inner Classes *****************************

  private final class AppIterator implements Iterator<App> {

    private final Iterator<File> appFiles;

    private App next;

    private AppIterator(final File[] appFiles) {
      this.appFiles = Arrays.stream(appFiles)
          .iterator();
    }

    @Override
    public boolean hasNext() {
      if (next == null) {
        while (appFiles.hasNext()) {
          final File file = appFiles.next();
          if (context.isAccepted(file)) {
            next = new App(file, context.getQualifierDateFormat());
            return true;
          }
        }
        return false;
      }
      return true;
    }

    @Override
    public App next() {
      if (next != null) {
        final App app = next;
        next = null;
        return app;
      }
      if (hasNext()) {
        return next();
      }

      throw new NoSuchElementException(
          "No next app available from this source.");
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static AppSourceFolder create(final FileAcceptorContext context,
      final File folder) throws NullPointerException, IllegalArgumentException {
    if (folder.canRead() && folder.isDirectory()) {
      final File[] appFiles = folder.listFiles(APP_FILENAME_FILTER);
      final AppSourceFolder appSource =
          new AppSourceFolder(context, folder, appFiles);
      context.logVerbose(appSource);
      return appSource;
    }

    throw new IllegalArgumentException(
        "Cannot read folder '" + folder.getAbsolutePath() + "'.");
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Iterator<App> iterator() {
    return new AppIterator(appFiles);
  }

  @Override
  public List<App> createAppList() {
    final List<App> apps = new ArrayList<>(appFiles.length);
    for (final App app : this) {
      apps.add(app);
    }
    return apps;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    final StringBuilder buffer = new StringBuilder(256);

    buffer.append("The following ")
        .append(appFiles.length)
        .append(" application files are available in ")
        .append(folder.getAbsolutePath())
        .append(": ");
    for (final File appFile : appFiles) {
      buffer.append("\n  > ")
          .append(appFile.getName());
    }

    return buffer.toString();
  }
}
