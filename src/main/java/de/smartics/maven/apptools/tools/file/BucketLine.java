/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.file;

import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppSource;

import org.apache.maven.shared.utils.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BucketLine implements Iterable<App> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final int size;

  private final Map<String, List<App>> buckets = new LinkedHashMap<>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public BucketLine(final List<String> bucketDescriptor,
      final AppSource appSource) {
    final List<App> apps = appSource.createAppList();
    this.size = apps.size();

    if (bucketDescriptor != null && !bucketDescriptor.isEmpty()) {
      for (final String matchString : bucketDescriptor) {
        if (matchString == null || StringUtils.isBlank(matchString)) {
          continue;
        }

        final Acceptor acceptor = Acceptor.create(matchString);
        final List<App> localApps = new ArrayList<>(32);
        for (final Iterator<App> i = apps.iterator(); i.hasNext();) {
          final App app = i.next();
          if (acceptor.isAccepted(app.getName())) {
            localApps.add(app);
            i.remove();
          }
        }
        buckets.put(matchString, localApps);
      }
    }

    buckets.put("REST", apps);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Iterator<App> iterator() {
    final List<App> apps = new ArrayList<>(size);
    for (final List<App> local : buckets.values()) {
      apps.addAll(local);
    }
    return apps.iterator();
  }

  public boolean isEmpty() {
    return size == 0;
  }

// --- object basics --------------------------------------------------------

}
