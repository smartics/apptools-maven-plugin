/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.file;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.tools.file.PluginKeyExtractor.Metadata.Builder;
import de.smartics.maven.apptools.tools.maven.VersionHelper;
import de.smartics.maven.apptools.tools.xml.XmlElementFetcher;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.shared.utils.StringUtils;
import org.xml.sax.SAXException;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 *
 */
public class PluginKeyExtractor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final boolean useObrManifest;

  private final boolean useJarManifest;

  @Nullable
  private final DateFormat timestampDateFormat;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PluginKeyExtractor() {
    this(null);
  }

  public PluginKeyExtractor(@Nullable final DateFormat timestampDateFormat) {
    this(true, true, timestampDateFormat);
  }

  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public PluginKeyExtractor(final boolean useObrManifest,
      final boolean useJarManifest, final DateFormat timestampDateFormat) {
    this.useObrManifest = useObrManifest;
    this.useJarManifest = useJarManifest;
    this.timestampDateFormat = timestampDateFormat;
  }

  // ****************************** Inner Classes *****************************


  private final class JarHelper {
    private final File file;

    private final boolean isObr;

    private JarHelper(final File file) {
      this.file = file;
      this.isObr = FilenameUtils.isExtension(file.getName(), "obr");
    }

    private Metadata extractMetadata() {
      try {
        final JarFile jarFile = new JarFile(file);
        final Metadata.Builder builder = Metadata.Builder.a();
        if (isObr) {
          extractFromObr(builder, jarFile);
        } else {
          extractFromJar(builder, jarFile);
        }

        return builder.build();
      } catch (final Exception e) {
        throw new AppException(
            "Cannot determine plugin key for app artifact '" +
            file.getAbsolutePath() + "': " + e.getMessage(), e);
      }
    }

    private void extractFromJar(final Builder builder, final JarFile jarFile)
        throws IOException {
      extractFromAtlassianPlugin(builder, jarFile);
      readFromManifest(builder, jarFile);
      readFromBuildProperties(builder, jarFile);
      readFromBuildXml(builder, jarFile);
      readProperties(builder, jarFile);
    }

    private void extractFromAtlassianPlugin(final Builder builder,
        final JarFile jarFile) {
      if (builder.isComplete()) {
        return;
      }

      if (timestampDateFormat != null) {
        final JarEntry atlassianPluginXml =
            jarFile.getJarEntry("atlassian-plugin.xml");
        if (null != atlassianPluginXml) {
          final String value =
              readValueFromXml(jarFile, atlassianPluginXml, "version");
          builder.initVersion(value);
          if (StringUtils.isNotBlank(value)) {
            final String timestampPart =
                VersionHelper.fetchTimestampPart(value);
            handleTimestamp(builder, timestampPart);
          }
        }
      }
    }

    private void extractFromObr(final Builder builder, final JarFile jarFile)
        throws IOException {
      if (builder.isComplete()) {
        return;
      }
      if (useObrManifest) {
        readFromManifest(builder, jarFile);
      }

      if (builder.isComplete()) {
        return;
      }
      final String jarFileName =
          FilenameUtils.getBaseName(file.getName()) + ".jar";
      final JarEntry jarFileEntry = jarFile.getJarEntry(jarFileName);
      if (jarFileEntry != null) {
        try (final InputStream in = new BufferedInputStream(
            jarFile.getInputStream(jarFileEntry))) {
          final JarFile includedJarFile = extractJarFile(in);
          extractFromJar(builder, includedJarFile);
        } catch (final IOException e) {
          throw new AppException(
              "Cannot fetch plugin key from '" + file.getAbsolutePath() +
              "' OBR's JAR file: " + e.getMessage());
        }
      } else {
        if (builder.isPluginKeyUnset()) {
          throw new AppException(
              "Cannot determine plugin key for app artifact '" +
              file.getAbsolutePath() +
              "' since it is not provided in the Manifest file and" +
              " there is no JAR archive inside this OBR named '" + jarFileName +
              " to consult.");
        }
      }
    }

    private JarFile extractJarFile(final InputStream jarFileInputStream)
        throws IOException {
      final File tmpFile = File.createTempFile("apptools-jar-tmp", ".jar");
      tmpFile.deleteOnExit();
      FileUtils.copyInputStreamToFile(jarFileInputStream, tmpFile);
      return new JarFile(tmpFile);
    }

    @CheckForNull
    private JarEntry findInJar(final JarFile jarFile, final String pluginKey,
        final String fileName) {
      if (StringUtils.isNotBlank(pluginKey)) {
        final String entryName = toPath(pluginKey, fileName);
        return jarFile.getJarEntry("META-INF/maven/" + entryName);
      }
      return bruteForcefindInJar(jarFile, fileName);
    }

    private JarEntry bruteForcefindInJar(final JarFile jarFile,
        final String fileName) {
      JarEntry selectedEntry = null;

      final Enumeration<JarEntry> enumEntries = jarFile.entries();
      while (enumEntries.hasMoreElements()) {
        final JarEntry currentEntry = enumEntries.nextElement();
        final String currentEntryName = currentEntry.getName();
        final int index = currentEntryName.lastIndexOf('/');
        if (index != -1 && index < currentEntryName.length() - 1) {
          final String shortName = currentEntryName.substring(index + 1);
          if (fileName.equals(shortName)) {
            if (null == selectedEntry) {
              selectedEntry = currentEntry;
            } else {
              // If there is more than one entry, we cannot tell which one is
              // correct without the plugin key.
              return null;
            }
          }
        }
      }
      return selectedEntry;
    }

    private void readProperties(final Metadata.Builder builder,
        final JarFile jarFile) throws IOException {
      if (builder.isComplete()) {
        return;
      }
      final JarEntry pomProperties =
          findInJar(jarFile, builder.pluginKey, "pom.properties");

      if (pomProperties != null) {
        final Properties properties = new Properties();
        try (final InputStream in = new BufferedInputStream(
            jarFile.getInputStream(pomProperties))) {
          properties.load(in);
          if (builder.isPluginKeyUnset()) {
            handlePluginKey(builder, properties, "groupId", "artifactId");
          }
          handleVersion(builder, properties, "version");
        }
      }
    }

    private void readFromManifest(final Metadata.Builder builder,
        final JarFile jarStream) throws IOException {
      if (builder.isComplete()) {
        return;
      }
      final Manifest manifest = jarStream.getManifest();
      if (manifest != null) {
        final Attributes main = manifest.getMainAttributes();
        if (main != null) {
          final String key = main.getValue("Bundle-SymbolicName");
          if (StringUtils.isNotBlank(key)) {
            builder.initPluginKey(key);
          }
          // The version found here has not the timestamp part in case of
          // SNAPSHOT versions. In case of OBRs we would not be able to
          // determine if the SNAPSHOT version is older. Therefore we do not
          // use the version here. We could combine the version here with the
          // timestamp in a future version.
          // see de.smartics.maven.apptools.tools.deploy.ConfluenceRestCheckDeployed.calculateVersion
          //          final String version = main.getValue("Bundle-Version");
          //          if (StringUtils.isNotBlank(version)) {
          //            builder.initVersion(version);
          //          }

          final Attributes buildMetaData =
              manifest.getAttributes("BuildMetaData");
          if (buildMetaData != null) {
            final String buildTimestampCustom =
                buildMetaData.getValue("build_timestamp_custom");
            handleTimestamp(builder, buildTimestampCustom);
          }

          if (builder.isArchiveDateUnset()) {
            final JarEntry jarEntry =
                jarStream.getJarEntry("META-INF/MANIFEST.MF");
            if (jarEntry != null) {
              final long timestamp = jarEntry.getLastModifiedTime().toMillis();
              builder.initArchiveDate(new Date(timestamp));
            }
          }
        }
      }
    }
  }

  private String toPath(final String pluginKey, final String fileName) {
    if (StringUtils.isNotBlank(pluginKey)) {
      final int index = pluginKey.lastIndexOf('.');
      if (index != -1 && index + 1 < pluginKey.length()) {
        return pluginKey.substring(0, index) + '/' +
               pluginKey.substring(index + 1) + '/' + fileName;
      }
    }

    return fileName;
  }

  private void handleTimestamp(final Builder builder,
      final String buildTimestampCustom) {
    if (timestampDateFormat != null && buildTimestampCustom != null) {
      try {
        final Date date = timestampDateFormat.parse(buildTimestampCustom);
        builder.initArchiveDate(date);
      } catch (final ParseException e) {
        // okay, date cannot be parsed, try other locations ...
      }
    }
  }


  private void readFromBuildProperties(final Metadata.Builder builder,
      final JarFile jarFile) throws IOException {
    if (builder.isComplete()) {
      return;
    }

    final JarEntry buildProperties =
        jarFile.getJarEntry("META-INF/build.properties");

    if (buildProperties != null) {
      final Properties properties = new Properties();
      try (final InputStream in = new BufferedInputStream(
          jarFile.getInputStream(buildProperties))) {
        properties.load(in);
        if (builder.isPluginKeyUnset()) {
          handlePluginKey(builder, properties, "build.groupId",
              "build.artifactId");
        }
        if (builder.isVersionUnset()) {
          handleVersion(builder, properties, "build.version");
        }

        if (builder.isArchiveDateUnset()) {
          final String millisString =
              properties.getProperty("build.timestamp.millis");
          if (null != millisString) {
            final long millis = Long.parseLong(millisString);
            builder.initArchiveDate(new Date(millis));
          } else {
            handleTimestamp(builder,
                properties.getProperty("build.timestamp.custom"));
          }
        }
      }
    }
  }

  private void readFromBuildXml(final Metadata.Builder builder,
      final JarFile jarFile) {
    if (builder.isComplete()) {
      return;
    }
    if (builder.isArchiveDateUnset()) {
      final JarEntry buildXml =
          jarFile.getJarEntry("META-INF/buildmetadata.xml");
      if (null != buildXml) {
        final String value = readValueFromXml(jarFile, buildXml, "timestamp");
        if (StringUtils.isNotBlank(value)) {
          final long millis = Long.parseLong(value);
          builder.initArchiveDate(new Date(millis));
        }
      }
    }
  }

  private static void handleVersion(final Builder builder,
      final Properties properties, final String versionKey) {
    final String version = properties.getProperty(versionKey);
    builder.initVersion(version);
  }

  private static void handlePluginKey(final Builder builder,
      final Properties properties, final String groupIdKey,
      final String artifactIdKey) {
    final String groupId = properties.getProperty(groupIdKey);
    if (StringUtils.isNotBlank(groupId)) {
      final String artifactId = properties.getProperty(artifactIdKey);
      if (StringUtils.isNotBlank(artifactId)) {
        builder.initPluginKey(groupId + '.' + artifactId);
      }
    }
  }


  public static final class Metadata {
    @Nullable
    private final Date archiveDate;
    @Nullable
    private final String pluginKey;
    @Nullable
    private final String version;

    private Metadata(final Builder builder) {
      this(builder.archiveDate, builder.pluginKey, builder.version);
    }

    private Metadata(final Date archiveDate, final String pluginKey,
        final String version) {
      this.archiveDate = archiveDate;
      this.pluginKey = pluginKey;
      this.version = version;
    }

    public static Metadata create(final Date archiveDate,
        final String pluginKey, final String version) {
      return new Metadata(archiveDate, pluginKey, version);
    }

    public static final class Builder {
      private Date archiveDate;
      private String pluginKey;
      private String version;

      public static Builder a() {
        return new Builder();
      }

      @SuppressFBWarnings("EI_EXPOSE_REP2")
      public void initArchiveDate(final Date archiveDate) {
        if (this.archiveDate == null && archiveDate != null) {
          this.archiveDate = archiveDate;
        }
      }

      public void initPluginKey(final String pluginKey) {
        if (this.pluginKey == null && StringUtils.isNotBlank(pluginKey)) {
          this.pluginKey = pluginKey;
        }
      }

      public void initVersion(final String version) {
        if (this.version == null && StringUtils.isNotBlank(version)) {
          this.version = version;
        }
      }

      public Metadata build() {
        return new Metadata(this);
      }

      public boolean isComplete() {
        return !isArchiveDateUnset() && !isPluginKeyUnset() &&
               !isVersionUnset();
      }

      public boolean isArchiveDateUnset() {
        return null == archiveDate;
      }


      public boolean isPluginKeyUnset() {
        return null == pluginKey;
      }

      public boolean isVersionUnset() {
        return null == version;
      }
    }

    @SuppressFBWarnings("EI_EXPOSE_REP")
    @Nullable
    public Date getArchiveDate() {
      return archiveDate;
    }

    @Nullable
    public String getPluginKey() {
      return pluginKey;
    }

    @Nullable
    public String getVersion() {
      return version;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public Metadata fetchMetadata(final File file) {
    checkFile(file);

    final JarHelper helper = new JarHelper(file);
    return helper.extractMetadata();
  }

  private void checkFile(final File file) {
    if (!(file.isFile() || file.canRead())) {
      throw new AppException("Cannot determine plugin key for app artifact '" +
                             file.getAbsolutePath() +
                             "' since that file cannot be read or is not a " +
                             "file.");
    }
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  private static String readValueFromXml(final JarFile jarFile,
      final JarEntry jarEntry, final String elementName) {
    if (jarEntry != null) {
      try (final InputStream in = new BufferedInputStream(
          jarFile.getInputStream(jarEntry))) {
        return new XmlElementFetcher().fetchValue(in, elementName);
      } catch (final ParserConfigurationException | SAXException |
                     IOException e) {
        // okay, return null
      }
    }

    return null;
  }

  // --- object basics --------------------------------------------------------

}
