/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.enable;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.RemoteAppException;
import de.smartics.maven.apptools.mojo.EnableMojo.Context;
import de.smartics.maven.apptools.tools.client.AbstractConfluenceRestClient;

import com.google.gson.Gson;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Enables apps on a remote Confluence server via a REST API.
 */
public class ConfluenceRestEnabler
    extends AbstractConfluenceRestClient<ConfluenceRestEnabler> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ConfluenceRestEnabler(final Builder<ConfluenceRestEnabler> builder) {
    super(builder);
  }

  // ****************************** Inner Classes *****************************

  public static final class EnablerBuilder
      extends Builder<ConfluenceRestEnabler> {
    public EnablerBuilder(final Context context) {
      super(context);
      servicePath("/rest/plugins/1.0/enablement-state/");
    }

    public ConfluenceRestEnabler build() {
      init();
      return new ConfluenceRestEnabler(this);

    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static EnablerBuilder a(final Context context) {
    return new EnablerBuilder(context);
  }

  // --- get&set --------------------------------------------------------------

  private Context getContext() {
    return (Context) context;
  }

  // --- business -------------------------------------------------------------

  public void enable(final EnablementPlugins plugins)
      throws RemoteAppException {
    if (!getContext().isDryRun()) {
      final String token = fetchToken();
      enableApps(token, plugins);
    } else {
      context.logInfo(
          "If not in dryRun mode, apps identified by the following keys would be enabled on '"
              + restServiceUrl + "': " + plugins);
    }
  }

  private void enableApps(final String token, final EnablementPlugins plugins)
      throws RemoteAppException {
    context.logVerbose("  Enabling " + plugins.size() + " apps ...");

    final HttpPut request = createDeployRequest(token, plugins);

    try (final CloseableHttpResponse response = client.execute(request)) {
      final int statusCode = response.getStatusLine()
          .getStatusCode();
      if (statusCode != 200) {
        throw new RemoteAppException(
            "Cannot enable apps: " + response.getStatusLine(), statusCode);
      }
      context.logInfo(
          "  Enablement request for apps has been successfully accepted by '"
              + restServiceUrl + "'.");
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException("Cannot enable apps on remote server.", e);
    }
  }

  private HttpPut createDeployRequest(final String token,
      final EnablementPlugins plugins) throws AppException {
    try {
      final HttpPut request = new HttpPut();
      request.setHeader(authHeader);
      request.setURI(new URI(restServiceUrl + "?token=" + token));
      // request.setHeader("Content-Type", value);
      final Gson gson = new Gson();
      final String json = gson.toJson(plugins);
      final StringEntity entity = new StringEntity(json,
          ContentType.create("application/vnd.atl.plugins+json"));
      if (context.isDebug()) {
        context.logDebug("JSON to be sent for enablement:\n" + json);
      }
      request.setEntity(entity);

      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to post app to REST service.", e);
    }
  }

  // --- object basics --------------------------------------------------------

}
