/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.enable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.CheckForNull;

/**
 *
 */
public class EnablementPlugins implements Iterable<EnablementPlugin> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private List<EnablementPlugin> plugins;

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void add(final EnablementPlugin plugin) {
    if (plugins == null) {
      plugins = new ArrayList<>();
    }
    plugins.add(plugin);
  }

  @Override
  public Iterator<EnablementPlugin> iterator() {
    if (plugins != null) {
      return plugins.iterator();
    } else {
      return Collections.<EnablementPlugin> emptyList()
          .iterator();
    }
  }

  public int size() {
    if (plugins != null) {
      return plugins.size();
    }
    return 0;
  }

  public boolean isEmpty() {
    return plugins == null || plugins.size() == 0;
  }

  @CheckForNull
  public EnablementPlugin fetch(final String key) {
    if (plugins != null) {
      for (final EnablementPlugin plugin : plugins) {
        if (key.equals(plugin.getKey())) {
          return plugin;
        }
      }
    }
    return null;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    if (plugins == null) {
      return "no plugins found";
    }
    final StringBuilder buffer = new StringBuilder(2048);
    for (final EnablementPlugin plugin : plugins) {
      buffer.append('\n')
          .append(plugin);
    }
    return buffer.toString();
  }
}
