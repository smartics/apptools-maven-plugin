/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.deploy;

import com.google.gson.Gson;
import de.smartics.maven.apptools.domain.AccessPlugins;
import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.Check;
import de.smartics.maven.apptools.domain.PluginAcceptorContext;
import de.smartics.maven.apptools.mojo.AbstractVersionUpmMojo.VersionUpmContext;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.resources.Plugins;
import de.smartics.maven.apptools.tools.client.AbstractConfluenceRestClient;
import de.smartics.maven.apptools.tools.maven.VersionHelper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.codehaus.mojo.versions.api.ArtifactVersions;
import org.codehaus.mojo.versions.api.DefaultVersionsHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;

/**
 * Checks via an REST API which apps have been installed on the remote
 * Confluence server.
 *
 * @see <a href= "https://ecosystem.atlassian
 * .net/wiki/spaces/UPM/pages/6094960/UPM+REST+API">UPM REST API</a>
 */
public class ConfluenceRestCheckDeployed
    extends AbstractConfluenceRestClient<ConfluenceRestCheckDeployed>
    implements Check, AccessPlugins {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ConfluenceRestCheckDeployed(final CheckBuilder builder) {
    super(builder);
  }

  // ****************************** Inner Classes *****************************


  public static final class CheckBuilder
      extends Builder<ConfluenceRestCheckDeployed> {

    public CheckBuilder(final PluginAcceptorContext context) {
      super(context);
      servicePath("/rest/plugins/1.0/");
      // "/rest/plugins/1.0/monitor/installed" may also work, but
      // https://ecosystem.atlassian.net/wiki/spaces/UPM/pages/6094960/UPM+REST+API
      // tells us to use "/rest/plugins/1.0/"
    }

    @Override
    public ConfluenceRestCheckDeployed build() {
      client = createHttpClient(timeoutMs, username, password);
      authHeader = createAuthHeader();
      return new ConfluenceRestCheckDeployed(this);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static CheckBuilder a(final PluginAcceptorContext context) {
    return new CheckBuilder(context);
  }

  // --- get&set --------------------------------------------------------------

  public VersionUpmContext getContext() {
    return (VersionUpmContext) context;
  }
  // --- business -------------------------------------------------------------

  @Override
  public void check() throws AppException {
    final String token = fetchToken();
    checkApps(token);
  }

  private void checkApps(final String token) throws AppException {
    context.logVerbose("  Checking apps on '" + restServiceUrl + "' ...");
    final Plugins plugins = fetchPlugins(token);
    final String format = calcFormat(plugins);
    final StringBuilder buffer = new StringBuilder(2048);
    final String legend =
        String.format(format, "?", "Plugin Key", "Current Plugin Version",
            "Latest Artifact Version");
    buffer.append('\n').append(legend);
    final DefaultVersionsHelper helper = getContext().getVersionsHelper();
    for (final Plugin plugin : plugins) {
      final String normalizedVersion =
          plugin.getNormalizedVersion(qualifierDateFormat);
      final Artifact artifact = createArtifact(plugin, normalizedVersion);
      if (artifact == null) {
        buffer.append('\n').append(
            String.format(format, plugin.isEnabled() ? "+" : "-",
                plugin.getKey(), plugin.getVersion(), "IS UNKNOWN"));
      } else {
        try {
          final ArtifactVersions versions =
              helper.lookupArtifactUpdates(artifact,
                  !getContext().isRequireReleases(), false);
          final ArtifactVersion version =
              VersionHelper.getNewestUpdate(artifact, versions);
          final String versionLabel =
              calculateVersion(plugin, artifact, version);
          buffer.append('\n').append(
              String.format(format, plugin.isEnabled() ? "+" : "-",
                  plugin.getKey(), plugin.getVersion(), versionLabel));
        } catch (final Exception e) {
          buffer.append('\n').append(
              String.format(format, plugin.isEnabled() ? "+" : "-",
                  plugin.getKey(), plugin.getVersion(),
                  ">FAILED<: " + e.getMessage()));
        }
      }
    }
    context.logInfo(
        "  Plugins deployed to application server for environment '" +
        context.getActiveEnvironmentProfile() +
        "' compared to artifacts on the artifact server: " + buffer);
  }

  private String calculateVersion(final Plugin plugin, final Artifact artifact,
      final ArtifactVersion version) {
    if (version == null) {
      final File file = artifact.getFile();
      if (file != null) {
        final App app = new App(file, qualifierDateFormat);
        final String pluginVersion = plugin.getVersion();
        final Date pluginDate = parseDateFromVersion(pluginVersion);
        final Date appDate = app.getCreationDate();
        if (appDate != null && pluginDate != null &&
            appDate.after(pluginDate)) {
          final String appVersion = app.getVersion();
          if (null != appVersion) {
            return appVersion.replace("SNAPSHOT",
                qualifierDateFormat.format(appDate));
          }
          return null;
        }
      }
      return "IS LATEST!";
    } else {
      return version.toString();
    }
  }

  private Date parseDateFromVersion(final String version) {
    try {
      final String timestampPart = VersionHelper.fetchTimestampPart(version);
      if (timestampPart != null) {
        return qualifierDateFormat.parse(timestampPart);
      }
    } catch (final ParseException e) {
      // okay, date cannot be parsed, try build properties ...
    }

    return null;
  }

  private Artifact createArtifact(final Plugin plugin,
      final String normalizedVersion) {
    Artifact artifact =
        getContext().createArtifact(plugin.getKey(), normalizedVersion, "obr");
    artifact = artifactDownloadHelper.download(artifact);

    if (artifact == null) {
      artifact = getContext().createArtifact(plugin.getKey(), normalizedVersion,
          "jar");
      artifact = artifactDownloadHelper.download(artifact);
    }

    return artifact;
  }

  private String calcFormat(final Plugins plugins) {
    int maxKey = 0;
    for (final Plugin plugin : plugins) {
      final String key = plugin.getKey();
      if (key != null) {
        maxKey = Math.max(key.length(), maxKey);
      }
    }

    return "%1s %" + (maxKey + 1) + "s  %-25s -> %25s";
  }


  @Override
  public Plugins fetchPlugins() throws AppException {
    final String token = fetchToken();
    return fetchPlugins(token);
  }

  @Override
  public Plugins fetchPlugins(final String token) throws AppException {
    context.logVerbose("  Fetching apps from '" + restServiceUrl + "' ...");
    final HttpGet request = createCheckRequest(token);

    try (final CloseableHttpResponse response = client.execute(request)) {
      if (response.getStatusLine().getStatusCode() != 200) {
        throw new AppException(
            "Cannot fetch information about deployed apps from remote '" +
            restServiceUrl + "': " + response.getStatusLine());
      }

      final HttpEntity entity = response.getEntity();

      try (final Reader in = new InputStreamReader(
          new BufferedInputStream(entity.getContent()),
          StandardCharsets.UTF_8)) {
        final Gson gson = new Gson();
        final Plugins plugins = gson.fromJson(in, Plugins.class);
        for (final Iterator<Plugin> i = plugins.iterator(); i.hasNext(); ) {
          final Plugin plugin = i.next();
          if (!getContext().isAccepted(plugin)) {
            i.remove();
          }
        }
        return plugins;
      }
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException(
          "Cannot fetch deployed apps via '" + restServiceUrl + "'.", e);
    }
  }

  private HttpGet createCheckRequest(final String token) throws AppException {
    try {
      final HttpGet request = new HttpGet();
      request.setHeader(authHeader);
      request.setHeader("Accept",
          "application/json,application/vnd.atl.plugins.installed+json");
      request.setURI(new URI(restServiceUrl + "?token=" + token));
      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to request apps installed on remote server " +
          "via '" + restServiceUrl + "'.", e);
    }
  }

  // --- object basics --------------------------------------------------------

}
