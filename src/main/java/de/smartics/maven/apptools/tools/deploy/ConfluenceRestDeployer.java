/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.deploy;

import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.DeployTarget;
import de.smartics.maven.apptools.domain.RemoteAppException;
import de.smartics.maven.apptools.mojo.DeployMojo.Context;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.tools.client.AbstractConfluenceRestClient;
import de.smartics.maven.apptools.tools.enable.PluginSummary;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Deploys apps to a remote Confluence server via a REST API.
 *
 * @see <a href=
 *      "https://developer.atlassian.com/platform/marketplace/registering-apps/">Registering
 *      apps</a>
 * @see <a href=
 *      "https://community.atlassian.com/t5/Answers-Developer-Questions/Install-plugin-into-Jira-using-UPM-REST-API/qaq-p/486119">Install
 *      plugin into Jira using UPM REST API</a>
 */
public class ConfluenceRestDeployer
    extends AbstractConfluenceRestClient<ConfluenceRestDeployer>
    implements DeployTarget {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ConfluenceRestDeployer(
      final Builder<ConfluenceRestDeployer> builder) {
    super(builder);
  }

  // ****************************** Inner Classes *****************************

  public static final class DeployerBuilder
      extends Builder<ConfluenceRestDeployer> {
    public DeployerBuilder(final Context context) {
      super(context);
      servicePath("/rest/plugins/1.0/");
    }

    @Override
    public ConfluenceRestDeployer build() {
      init();
      return new ConfluenceRestDeployer(this);

    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static DeployerBuilder a(final Context context) {
    return new DeployerBuilder(context);
  }

  // --- get&set --------------------------------------------------------------

  private Context getContext() {
    return (Context) context;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void deploy(final App app, final Plugin expectedPlugin)
      throws AppException {
    if (!getContext().isDryRun()) {
      final String token = fetchToken();
      deployApp(token, app);
      final String pluginKey = app.getKey();
      if (null != pluginKey && getContext().enablementCheckRequired(pluginKey)) {
        context.logVerbose("Checking if app '" + app.getName() + "' with key '"
            + pluginKey + " has already been enabled ...");
        final PluginSummary plugin = new PluginSummary(pluginKey,
            expectedPlugin != null ? expectedPlugin.getVersion() : null, true);
        final long wait = getContext().getCheckWaitMs();
        final int max = getContext().getCheckMaxCount();
        int count = 0;
        while (checkEnablementState(plugin)) {
          try {
            if (count++ >= max) {
              throw new AppException(
                  "Cannot get a positive response on enablement state of '"
                      + pluginKey + "' after " + max + " tries each " + wait
                      + " ms.");
            }
            context.logVerbose("#" + count + ": Sleeping " + wait + " ms for '"
                + pluginKey + "' being enabled ...");
            Thread.sleep(wait);
          } catch (final AppException e) {
            throw e;
          } catch (final Exception e) {
            // Continue
          }
        }
        context.logInfo("App '" + app.getName() + "' with key '" + pluginKey
            + "' has been enabled on remote server!");
      }
    } else {
      context.logInfo("If not in dryRun mode, app '" + app.getName()
          + "' would be deployed to '" + restServiceUrl + "'.");
    }
  }

  private boolean checkEnablementState(final PluginSummary plugin) {
    try {
      return !checker.isInState(plugin);
    } catch (final RemoteAppException e) {
      final int statusCode = e.getHttpStatusCode();
      if (statusCode >= 500) {
        getContext().logWarn(
            "Continuing although remote server signalled an internal failure '"
                + statusCode + "': " + e.getMessage());
        return true;
      }
      throw e;
    }
  }

  private void deployApp(final String token, final App app)
      throws AppException {
    context.logVerbose("  Deploying app '" + app.getName() + "' ...");
    final HttpPost request = createDeployRequest(token, app);

    try (final CloseableHttpResponse response = client.execute(request)) {
      if (response.getStatusLine()
          .getStatusCode() != 202) {
        throw new AppException("Cannot post app " + app.getName() + ": "
            + response.getStatusLine());
      }
      context.logInfo("  Deployment request for app '" + app.getName()
          + "' successfully accepted by '" + restServiceUrl + "'.");
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException(
          "Cannot post app " + app.getName() + " to REST service.", e);
    }
  }

  private HttpPost createDeployRequest(final String token, final App app)
      throws AppException {
    try {
      final HttpPost request = new HttpPost();
      request.setHeader(authHeader);
      request.setHeader("Accept", "application/json");
      // request.setHeader("Content-Type",
      // "application/vnd.atl.plugins.install.uri+json");
      request.setURI(new URI(restServiceUrl + "?token=" + token));
      request.setEntity(MultipartEntityBuilder.create()
          .addBinaryBody("plugin", app.getFile())
          .build());
      return request;
    } catch (final URISyntaxException e) {
      throw new AppException(
          "Cannot create request to post app to REST service.", e);
    }
  }

  // --- object basics --------------------------------------------------------

}
