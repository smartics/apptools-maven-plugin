/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.file;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Functions to deal with the files on a file system.
 */
public final class FileUtilities {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static File createFolder(final File baseDir, final String sourceFolder)
      throws FileNotFoundException {
    File dir = new File(sourceFolder);
    if (isReadableDirectory(dir)) {
      return dir;
    }

    if (baseDir != null) {
      dir = new File(baseDir, sourceFolder);
      if (isReadableDirectory(dir)) {
        return dir;
      } else {
        if (!dir.exists()) {
          if (!dir.mkdirs()) {
            throw new FileNotFoundException("The folder " + sourceFolder +
                                            " and its subfolders cannot be " +
                                            "created.");
          }
          return dir;
        } else {
          throw new FileNotFoundException("The folder " + sourceFolder +
                                          " does not point to a valid folder " +
                                          "nor does " + dir.getAbsolutePath() +
                                          ".");
        }
      }
    }

    if (!dir.exists()) {
      if (!dir.mkdirs()) {
        throw new FileNotFoundException(
            "Cannot create folder " + sourceFolder + " and subfolders.");
      }
      return dir;
    } else {
      throw new FileNotFoundException("The folder " + sourceFolder +
                                      " does not point to a valid folder nor " +
                                      "is a base folder specified.");
    }
  }

  public static boolean isReadableDirectory(final File dir) {
    return dir.isDirectory() && dir.canRead();
  }

  public static File absolutePathDir(final String sourceFolder) {
    final File dir = new File(sourceFolder);
    if (!dir.isAbsolute()) {
      return new File(System.getProperty("user.dir"), sourceFolder);
    }
    return dir;
  }

  public static boolean isEmpty(File dir) {
    if (dir.isDirectory() && dir.canRead()) {
      final String[] children = dir.list();
      return (children == null || children.length == 0);
    }
    return false;
  }

  // --- object basics --------------------------------------------------------

}
