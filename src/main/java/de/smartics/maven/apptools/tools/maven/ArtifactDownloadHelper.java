/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.tools.maven;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.shared.artifact.ArtifactCoordinate;
import org.apache.maven.shared.artifact.DefaultArtifactCoordinate;
import org.apache.maven.shared.artifact.resolve.ArtifactResolver;
import org.apache.maven.shared.artifact.resolve.ArtifactResolverException;

import java.util.List;

/**
 * Helper to download artifacts from repositories.
 */
public class ArtifactDownloadHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final MavenSession mavenSession;

  private final Log log;

  private final ArtifactResolver artifactResolver;

  private final List<ArtifactRepository> remoteRepositories;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public ArtifactDownloadHelper(final MavenSession mavenSession, final Log log,
      final ArtifactResolver artifactResolver,
      final List<ArtifactRepository> remoteRepositories) {
    this.mavenSession = mavenSession;
    this.log = log;
    this.artifactResolver = artifactResolver;
    this.remoteRepositories = remoteRepositories;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public Artifact download(final Artifact artifact) {
    final ArtifactCoordinate coordinate = calcArtifactCoordinates(artifact);
    return download(coordinate);
  }

  public Artifact download(final ArtifactCoordinate artifactCoordinate) {
    final ProjectBuildingRequest buildingRequest = createRequest();
    try {
      final Artifact resolvedArtifact =
          artifactResolver.resolveArtifact(buildingRequest, artifactCoordinate)
              .getArtifact();
      if (resolvedArtifact != null && resolvedArtifact.isResolved()) {
        return resolvedArtifact;
      }
    } catch (final ArtifactResolverException e) {
      if (log.isDebugEnabled()) {
        log.debug("Cannot resolve artifact '" + artifactCoordinate + "': "
            + e.getMessage(), e);
      }
      // return null later
    }
    return null;
  }

  private ProjectBuildingRequest createRequest() {
    final ProjectBuildingRequest buildingRequest =
        new DefaultProjectBuildingRequest(
            mavenSession.getProjectBuildingRequest());
    buildingRequest.setRemoteRepositories(remoteRepositories);
    return buildingRequest;
  }

  private ArtifactCoordinate calcArtifactCoordinates(final Artifact artifact) {
    final DefaultArtifactCoordinate coordinate =
        new DefaultArtifactCoordinate();
    coordinate.setGroupId(artifact.getGroupId());
    coordinate.setArtifactId(artifact.getArtifactId());
    coordinate.setExtension(artifact.getType());
    coordinate.setVersion(artifact.getVersion());
    return coordinate;
  }

  // --- object basics --------------------------------------------------------

}
