/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.resources;

import de.smartics.maven.apptools.tools.maven.VersionHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import javax.annotation.Nullable;

/**
 *
 */
public class Plugin {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private String key;

  private String name;

  private String version;

  private boolean enabled;

  private Long releaseDate;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Constructor for injection.
   */
  public Plugin() {}

  /**
   * Constructor for manual construction.
   */
  private Plugin(final String key, final String name, final String version,
      final boolean enabled, final Long releaseDate) {
    this.key = key;
    this.name = name;
    this.version = version;
    this.enabled = enabled;
    this.releaseDate = releaseDate;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static Plugin create(final String key, final String name,
      final String version, final boolean enabled, final Long releaseDate) {
    return new Plugin(key, name, version, enabled, releaseDate);
  }

  public static Plugin create(final String key, final String name,
      final String version) {
    return new Plugin(key, name, version, true, null);
  }

  // --- get&set --------------------------------------------------------------

  public String getKey() {
    return key;
  }

  public String getName() {
    return name;
  }

  public String getVersion() {
    return version;
  }

  public String getNormalizedVersion(DateFormat qualifierDateFormat) {
    final String version = getVersion();

    try {
      final String timestampPart = VersionHelper.fetchTimestampPart(version);
      if (timestampPart != null) {
        qualifierDateFormat.parse(timestampPart);
        return version.substring(0, version.length() - timestampPart.length())
            + "SNAPSHOT";
      }
    } catch (final ParseException e) {
      // okay, version is returned as specified ...
    }

    return version;
  }

  public boolean isEnabled() {
    return enabled;
  }

  @Nullable
  public Date getReleaseDate() {
    if (releaseDate != null) {
      return new Date(releaseDate);
    }
    return null;
  }

  public void setReleaseDate(final Long releaseDate) {
    this.releaseDate = releaseDate;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "enabled=" + enabled + " > " + key + ":" + version;
  }
}
