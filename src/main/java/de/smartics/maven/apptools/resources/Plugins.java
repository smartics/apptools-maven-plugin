/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.resources;

import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.tools.maven.VersionHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class Plugins implements Iterable<Plugin> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private List<Plugin> plugins;

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Constructor for injection.
   */
  public Plugins() {
  }

  /**
   * Constructor for manual construction.
   */
  private Plugins(final List<Plugin> plugins) {
    this.plugins = plugins;
  }

  // ****************************** Inner Classes *****************************


  public final class PluginMap {
    private final String qualifierDatePattern;
    private final Map<String, Plugin> map = new HashMap<>();

    private PluginMap(final String qualifierDatePattern) {
      this.qualifierDatePattern = qualifierDatePattern;
      for (final Plugin plugin : Plugins.this) {
        final String key = plugin.getKey();
        map.put(key, plugin);
      }
    }

    @Nullable
    public Plugin getPlugin(final String key) {
      return map.get(key);
    }

    /**
     * Checks if the given app is up-to-date with the deployed version on the
     * application server.
     *
     * @param app the app to check.
     * @return <code>true</code> if the app is up-to-date, <code>false</code> if
     * not or if it cannot be determined.
     */
    public boolean isUpToDate(final App app) {
      final String pluginKey = app.getKey();
      if (StringUtils.isNotBlank(pluginKey)) {
        final Plugin plugin = getPlugin(pluginKey);
        if (plugin != null) {
          final String pluginVersionString = plugin.getVersion();
          final String appVersionString = app.getVersion();
          if (StringUtils.isNotBlank(pluginVersionString) &&
              StringUtils.isNotBlank(appVersionString)) {
            final ArtifactVersion pluginVersion =
                new DefaultArtifactVersion(pluginVersionString);
            final VersionHelper versionHelper =
                new VersionHelper(qualifierDatePattern, app);
            final ArtifactVersion appVersion =
                versionHelper.createVersion(pluginVersion, appVersionString);
            final int versionResult = pluginVersion.compareTo(appVersion);
            final boolean pluginVersionIsQualified =
                versionHelper.isQualified(pluginVersion);
            final boolean appVersionIsQualified =
                versionHelper.isQualified(appVersion);
            if (pluginVersionIsQualified) {
              if (appVersionIsQualified) {
                return versionResult >= 0;
              } else {
                final int unqualifiedVersionResult =
                    VersionHelper.compareNonQualified(pluginVersion,
                        appVersion);
                if (unqualifiedVersionResult == 0) {
                  return false;
                } else {
                  return unqualifiedVersionResult > 0;
                }
              }
            } else {
              if (!appVersionIsQualified) {
                // Both are not qualified, the result requires no date
                return versionResult >= 0;
              }
            }
            if (versionResult != 0) {
              return versionResult > 0;
            }
          }

          // The plugin date precision is only day (Thu Feb 28 00:00:00 CET
          // 2019), while the app is seconds (Thu Feb 28 16:39:28 CET 2019)
          final Date pluginDate = normalizeDate(plugin.getReleaseDate());
          final Date appDate = normalizeDate(app.getCreationDate());
          return appDate != null && pluginDate != null &&
                 !appDate.after(pluginDate);
        }
      }

      return false;
    }

    private Date normalizeDate(final Date date) {
      if (date == null) {
        return null;
      }
      final Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      calendar.set(Calendar.HOUR_OF_DAY, 0);
      calendar.set(Calendar.MINUTE, 0);
      calendar.set(Calendar.SECOND, 0);
      calendar.set(Calendar.MILLISECOND, 0);
      return calendar.getTime();
    }

    public String getReleaseDate(final App app) {
      final String pluginKey = app.getKey();
      final Plugin plugin = map.get(pluginKey);
      if (plugin != null) {
        final Date pluginDate = plugin.getReleaseDate();
        if (pluginDate != null) {
          return pluginDate.toString();
        }
      }

      return "n/a";
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static Plugins create(final Plugin... plugins) {
    return new Plugins(Arrays.asList(plugins));
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Iterator<Plugin> iterator() {
    if (plugins != null) {
      return plugins.iterator();
    } else {
      return Collections.emptyIterator();
    }
  }

  public PluginMap getPluginMap(final String qualifierDatePattern) {
    return new PluginMap(qualifierDatePattern);
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    if (plugins == null) {
      return "no plugins found";
    }
    final StringBuilder buffer = new StringBuilder(2048);
    for (final Plugin plugin : plugins) {
      buffer.append('\n').append(plugin);
    }
    return buffer.toString();
  }
}
