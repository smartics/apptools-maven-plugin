/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.resources;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 *
 */
public class VersionDetails {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private Long releaseDate;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @SuppressFBWarnings(value = "UWF_UNWRITTEN_FIELD", justification = "The " +
                                                                     "field " +
                                                                     "is set " +
                                                                     "by a " +
                                                                     "GSON " +
                                                                     "parser.")
  public Long getReleaseDate() {
    return releaseDate;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
