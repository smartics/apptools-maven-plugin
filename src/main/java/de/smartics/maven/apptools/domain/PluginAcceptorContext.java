/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.domain;

import de.smartics.maven.apptools.resources.Plugin;

/**
 *
 */
public interface PluginAcceptorContext extends ExecutionContext {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  String MODE_LOCAL = "LOCAL";
  String MODE_REMOTE = "REMOTE";
  String MODE_BOTH = "BOTH";

  // ****************************** Initializer *******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  boolean isAccepted(Plugin plugin);


  static boolean isLocal(final String mode) {
    return (mode == null || MODE_LOCAL.equals(mode) || MODE_BOTH.equals(mode));
  }

  static boolean isRemote(final String mode) {
    return (MODE_REMOTE.equals(mode) || MODE_BOTH.equals(mode));
  }

// --- object basics --------------------------------------------------------

}
