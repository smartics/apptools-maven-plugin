/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.domain;

import de.smartics.maven.apptools.tools.file.PluginKeyExtractor;
import de.smartics.maven.apptools.tools.file.PluginKeyExtractor.Metadata;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import javax.annotation.Nullable;

/**
 * A reference to an app and its meta data.
 */
public class App {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final File appFile;

  @Nullable
  private final DateFormat timestampDateFormat;

  private transient Metadata metadata;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   * @param appFile a reference to the application file.
   */
  public App(final File appFile,
      @Nullable final DateFormat timestampDateFormat) {
    this(appFile, timestampDateFormat, null);
  }

  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public App(final File appFile, @Nullable final DateFormat timestampDateFormat,
      final Metadata metadata) {
    this.appFile = appFile;
    this.timestampDateFormat = timestampDateFormat;
    this.metadata = metadata;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public File getFile() {
    return appFile;
  }

  public String getName() {
    return appFile.getName();
  }

  @Nullable
  public String getKey() {
    provideMetadata();
    return metadata.getPluginKey();
  }

  @Nullable
  public Date getCreationDate() {
    provideMetadata();
    return metadata.getArchiveDate();
  }

  @Nullable
  public String getVersion() {
    provideMetadata();
    return metadata.getVersion();
  }

  protected void provideMetadata() {
    if (metadata == null) {
      final PluginKeyExtractor helper =
          new PluginKeyExtractor(timestampDateFormat);
      metadata = helper.fetchMetadata(appFile);
    }
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return appFile.getAbsolutePath();
  }
}
