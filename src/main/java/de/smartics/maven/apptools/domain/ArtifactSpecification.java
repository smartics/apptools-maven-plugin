/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.domain;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.shared.artifact.ArtifactCoordinate;
import org.apache.maven.shared.artifact.DefaultArtifactCoordinate;

/**
 *
 */
public final class ArtifactSpecification {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String groupId;

  private final String artifactId;

  private final String extension;

  private final String version;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ArtifactSpecification(final String groupId, final String artifactId,
      final String extension, final String version) {
    this.groupId = groupId;
    this.artifactId = artifactId;
    this.extension = extension;
    this.version = version;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  @SuppressFBWarnings("SF_SWITCH_FALLTHROUGH")
  public static ArtifactSpecification fromString(
      final String specificationString) {
    final String[] parts = specificationString.split(":");
    String groupId, artifactId, version = null, extension = "jar";

    switch (parts.length) {
      case 4:
        groupId = parts[0];
        artifactId = parts[1];
        extension = parts[2];
        version = parts[3];
        break;
      case 3:
        version = parts[2];
      case 2:
        groupId = parts[0];
        artifactId = parts[1];
        break;
      case 1:
      default:
        throw new IllegalArgumentException(
            "Specification format is groupId:artifactId:extension:version but specified was: "
                + specificationString);
    }

    return new ArtifactSpecification(groupId, artifactId, extension, version);
  }

  // --- get&set --------------------------------------------------------------

  public String getArtifactId() {
    return artifactId;
  }

  // --- business -------------------------------------------------------------

  public ArtifactCoordinate createArtifactCoordinate(
      final String overrideVersion) {
    final DefaultArtifactCoordinate coordinate =
        new DefaultArtifactCoordinate();
    coordinate.setGroupId(groupId);
    coordinate.setArtifactId(artifactId);
    coordinate.setExtension(extension);
    coordinate.setVersion(overrideVersion == null ? version : overrideVersion);

    return coordinate;
  }

  public Artifact createArtifact(final ArtifactHandler handler) {
    return new DefaultArtifact(groupId, artifactId,
        version, null, extension, null, handler);
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return groupId + ':' + artifactId
        + (!"jar".equals(extension) ? ':' + extension : "")
        + (version != null ? ':' + version : "");
  }
}
