/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.AppSource;
import de.smartics.maven.apptools.tools.file.FileUtilities;
import de.smartics.maven.apptools.tools.source.AppSourceFolder;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * List information on the locally stored artifacts.
 * <p>
 * This lists all artifact files in the source folder with the the keys of the
 * contained apps.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "list", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class ListMojo extends AbstractArtifactMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public final class Context extends BaseArtifactContext {

    private Context(final File sourceFolder) {
      super(sourceFolder);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected boolean isDryRunSetOnTheMojo() {
    return false;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping listing apps since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      context.logVerbose("Starting listing apps with context:\n" + context);
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      throw new MojoExecutionException(e.getMessage());
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    // This mojo does not interact with the server, therefore the init() method
    // must not be called.
    // init();

    try {
      final String sourceFolderString = getSourceFolder();
      final File sourceFolder = sourceFolderString != null ?
                                FileUtilities.createFolder(project.getBasedir(),
                                    sourceFolderString) : null;
      return new Context(sourceFolder);
    } catch (final FileNotFoundException e) {
      throw new AppException("Cannot find apps folder.", e);
    }
  }

  private void run(final Context context)
      throws MojoExecutionException, MojoFailureException {
    final File sourceFolder = context.getSourceFolder();
    if (sourceFolder == null) {
      context.logInfo(
          "No source folder specified. No apps found. Please specify a source" +
          " folder by the source parameter.");
      return;
    }
    final StringBuilder buffer = new StringBuilder(512);
    final AppSource appSource = AppSourceFolder.create(context, sourceFolder);
    final String format = calcFormat(appSource);
    for (final App app : appSource) {
      final String line = String.format(format, app.getName(), app.getKey());
      buffer.append('\n').append(line);
    }

    context.logInfo(
        "Locally stored app artifacts (JAR/OBR) and their plugin keys:\n" +
        buffer);
  }

  private String calcFormat(final AppSource appSource) {
    int maxFilename = 0;
    int maxKey = 0;
    for (final App app : appSource) {
      final String key = app.getKey();
      if (key != null) {
        maxKey = Math.max(key.length(), maxKey);
      }
      final String filename = app.getName();
      if (filename != null) {
        maxFilename = Math.max(filename.length(), maxFilename);
      }
    }

    return "%" + (maxFilename + 1) + "s -> %-" + (maxKey + 1) + "s";
  }

  // --- object basics --------------------------------------------------------

}
