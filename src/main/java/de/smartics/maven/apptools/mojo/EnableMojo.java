/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import static de.smartics.maven.apptools.domain.PluginAcceptorContext.MODE_LOCAL;
import static de.smartics.maven.apptools.domain.PluginAcceptorContext.isLocal;
import static de.smartics.maven.apptools.domain.PluginAcceptorContext.isRemote;

import de.smartics.maven.apptools.domain.AccessPlugins;
import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.AppSource;
import de.smartics.maven.apptools.domain.PluginAcceptorContext;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.resources.Plugins;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestCheckDeployed;
import de.smartics.maven.apptools.tools.enable.ConfluenceRestEnabler;
import de.smartics.maven.apptools.tools.enable.EnablementPlugin;
import de.smartics.maven.apptools.tools.enable.EnablementPlugins;
import de.smartics.maven.apptools.tools.file.BucketLine;
import de.smartics.maven.apptools.tools.file.FileUtilities;
import de.smartics.maven.apptools.tools.source.AppSourceFolder;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Enable apps on the Confluence server.
 * <p>
 * Either specify the apps you want to enable explicitly by their plugin keys
 * (using the 'pluginKeys' parameter) or choose to find the keys in the
 * artifacts found in the folder specified by the 'sourceFolder' parameter.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "enable", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class EnableMojo extends AbstractArtifactMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * A simple flag to run on the specified environment, but without actually
   * enabling apps on the target.
   * <p>
   * User property: <tt>apptools.dryRun</tt>, <tt>dryRun</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean dryRun;

  /**
   * The path to the service on the server.
   * <p>
   * User property: <tt>atlassian.enablementStateServicePath</tt>,
   * <tt>enablementStateServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/enablement-state/</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String enablementStateServicePath;

  /**
   * The path to the service on the server to check which apps are deployed.
   * <p>
   * User property: <tt>atlassian.deployedAppsServicePath</tt>,
   * <tt>deployedAppsServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/monitor/installed</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String deployedAppsServicePath;

  /**
   * List of key for plugins to enable. If plugins are explicitly specified,
   * local artifacts in the source folder are not checked.
   * <p>
   * User property: <tt>apptools.pluginKeys</tt>, <tt>pluginKeys</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> pluginKeys;

//  /**
//   * Tells the client to enable (<code>true</code>) or disable
//   * (<code>false</code>) the apps on the remote server.
//   *
//   * @since 1.0
//   */
//  @Parameter(property = "enable", defaultValue = "true")
  private boolean enable = true;

  /**
   * Specify whether to extract keys from the local artifacts
   * (<code>LOCAL</code>), fetch keys from the remote server
   * (<code>REMOTE</code>) or both (<code>BOTH</code>). Note that
   * <code>includes</code> and <code>excludes</code> are applied in either case.
   * <p>
   * User property: <tt>apptools.mode</tt>, <tt>mode</tt>
   * </p>
   * <p>
   * Defaults to '<tt>LOCAL</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String mode;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public final class Context extends BaseArtifactContext
      implements PluginAcceptorContext {

    private Context(final File sourceFolder) {
      super(sourceFolder);
    }

    @Override
    public boolean isAccepted(final Plugin plugin) {
      return super.isAccepted(plugin.getKey());
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected boolean isDryRunSetOnTheMojo() {
    return dryRun;
  }

  public String getEnablementStateServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "enablementStateServicePath", enablementStateServicePath,
        "/rest/plugins/1.0/enablement-state/");
  }

  public String getDeployedAppsServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "deployedAppsServicePath", deployedAppsServicePath,
        "/rest/plugins/1.0/monitor/installed");
  }

  public List<String> getPluginKeys() {
    return getUserPropertyAsList("pluginKeys", pluginKeys);
  }

  public boolean isEnable() {
    return enable;
  }

  public String getMode() {
    return getUserPropertyAsString("mode", mode, MODE_LOCAL);
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping enablement check of apps since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      context.logVerbose("Starting enablement with context:\n" + context
          + "\n  order: " + getOrder());
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      throw new MojoExecutionException(e.getMessage());
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    init();

    final String sourceFolderString = getSourceFolder();
    try {
      final File sourceFolder = sourceFolderString != null
          ? FileUtilities.createFolder(project.getBasedir(), sourceFolderString)
          : null;
      return new Context(sourceFolder);
    } catch (final FileNotFoundException e) {
      throw new AppException("Cannot find apps folder.", e);
    }
  }

  private void run(final Context context)
      throws MojoExecutionException, MojoFailureException {
    final EnablementPlugins plugins = new EnablementPlugins();
    final List<String> pluginKeys = getPluginKeys();
    final String mode = getMode();
    final boolean enable = isEnable();
    final List<String> includes = getIncludes();
    final List<String> excludes = getExcludes();
    if (pluginKeys != null && !pluginKeys.isEmpty()) {
      for (final String key : pluginKeys) {
        final EnablementPlugin plugin = new EnablementPlugin(key, enable);
        plugins.add(plugin);
      }
    } else if (isLocal(mode) && context.getSourceFolder() != null) {
      final AppSource appSource =
          AppSourceFolder.create(context, context.getSourceFolder());
      final List<String> order = getOrder();
      final BucketLine line = new BucketLine(order, appSource);
      for (final App app : line) {
        final String key = app.getKey();
        final EnablementPlugin plugin = new EnablementPlugin(key, enable);
        plugins.add(plugin);
      }
    } else if (isRemote(mode)
        && (isNotEmpty(includes) || isNotEmpty(excludes))) {
      try (final AccessPlugins access = ConfluenceRestCheckDeployed.a(context)
          .username(getUsername())
          .password(getPassword())
          .serverUrl(getServerUrl())
          .tokenServicePath(getTokenServicePath())
          .servicePath(getDeployedAppsServicePath())
          .timeoutMs(getTimeoutMs())
          .build()) {
        final Plugins remotePlugins = access.fetchPlugins();
        for (final Plugin remotePlugin : remotePlugins) {
          final String pluginKey = remotePlugin.getKey();
          final EnablementPlugin plugin =
              new EnablementPlugin(pluginKey, enable);
          plugins.add(plugin);
        }
      } catch (final Exception e) {
        throw new AppException(
            "Failed to undeploy apps from remote server: " + e.getMessage(), e);
      }
    }

    if (plugins.isEmpty()) {
      context.logInfo("No plugins specified to enable. Quitting ...");
      return;
    }

    context.logVerbose("Changing status for the following apps: " + plugins);

    try (final ConfluenceRestEnabler target = ConfluenceRestEnabler.a(context)
        .username(getUsername())
        .password(getPassword())
        .serverUrl(getServerUrl())
        .tokenServicePath(getTokenServicePath())
        .servicePath(getEnablementStateServicePath())
        .timeoutMs(getTimeoutMs())
        .build()) {
      target.enable(plugins);
    } catch (final Exception e) {
      throw new AppException(
          "Failed to enable apps on remote server: " + e.getMessage(), e);
    }
  }

// --- object basics --------------------------------------------------------

}
