/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.Check;
import de.smartics.maven.apptools.domain.PluginAcceptorContext;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestCheckDeployed;
import de.smartics.maven.apptools.tools.file.Acceptor;
import de.smartics.maven.apptools.tools.maven.ArtifactDownloadHelper;

import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.artifact.resolve.ArtifactResolver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Check which apps are deployed on the Confluence server.
 * <p>
 * Compare the artifacts on the Confluence server with those on the artifact
 * server.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "check-deployment", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class CheckDeploymentMojo extends AbstractVersionUpmMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The path to the service on the server.
   * <p>
   * User property: <tt>atlassian.pluginsServicePath</tt>,
   * <tt>pluginsServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String pluginsServicePath;
  // "/rest/plugins/1.0/monitor/installed" may also work, but
  // https://ecosystem.atlassian.net/wiki/spaces/UPM/pages/6094960/UPM+REST+API
  // tells us to use "/rest/plugins/1.0/"

  /**
   * Strings to match apps keys to be listed. If empty, everything is included.
   * Suffix searches are supported by appending the dollar character ($) to the
   * include string.
   * <p>
   * User property: <tt>apptools.includes</tt>, <tt>includes</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> includes;

  /**
   * Strings to match in app key to be excluded. If empty, nothing is excluded.
   * Suffix searches are supported by appending the dollar character ($) to the
   * include string.
   * <p>
   * User property: <tt>apptools.excludes</tt>, <tt>excludes</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> excludes;

  /**
   * The pattern used to parse qualifier from the version reported by the
   * deployed plugin. If the qualifier does match, the versions of the deployed
   * plugin and the local apps are compared based on this date information.
   * <p>
   * User property: <tt>apptools.qualifierDatePattern</tt>,
   * <tt>qualifierDatePattern</tt>
   * </p>
   * <p>
   * Defaults to 'yyyyMMdd.HHmmss'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String qualifierDatePattern;

  /**
   * List of remote artifact repositories.
   */
  @Parameter(defaultValue = "${project.remoteArtifactRepositories}",
      readonly = true, required = true)
  protected List<ArtifactRepository> remoteRepositories;

  @Component
  protected ArtifactResolver artifactResolver;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public final class Context extends VersionUpmContext
      implements PluginAcceptorContext {
    private final Acceptor acceptor;

    private Context() {
      acceptor = Acceptor.create(getIncludes(), getExcludes());
    }

    @Override
    public boolean isAccepted(final Plugin plugin) {
      return acceptor.isAccepted(plugin.getKey());
    }

    public DateFormat getQualifierDateFormat() {
      return new SimpleDateFormat(getQualifierDatePattern());
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected boolean isDryRunSetOnTheMojo() {
    return false;
  }

  public String getPluginsServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "pluginsServicePath", pluginsServicePath, "/rest/plugins/1.0/");
  }

  public List<String> getIncludes() {
    return getUserPropertyAsList("includes", includes);
  }

  public List<String> getExcludes() {
    return getUserPropertyAsList("excludes", excludes);
  }

  public String getQualifierDatePattern() {
    return getUserPropertyAsString("qualifierDatePattern", qualifierDatePattern,
        "yyyyMMdd.HHmmss");
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping check deployment status of apps since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      throw new MojoExecutionException(e.getMessage(), e);
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    init();
    return new Context();
  }

  private void run(final Context context)
      throws MojoExecutionException, MojoFailureException {
    final DateFormat qualifierDateFormat =
        new SimpleDateFormat(getQualifierDatePattern());
    final ArtifactDownloadHelper artifactDownloadHelper =
        new ArtifactDownloadHelper(mavenSession, getLog(), artifactResolver,
            remoteRepositories);
    try (final Check check = ConfluenceRestCheckDeployed.a(context)
        .username(getUsername())
        .password(getPassword())
        .serverUrl(getServerUrl())
        .tokenServicePath(getTokenServicePath())
        .servicePath(getPluginsServicePath())
        .qualifierDateFormat(qualifierDateFormat)
        .artifactDownloadHelper(artifactDownloadHelper)
        .timeoutMs(getTimeoutMs())
        .build()) {
      check.check();
    } catch (final Exception e) {
      throw new AppException(
          "Failed to check deployment status of apps on remote server: "
              + e.getMessage(),
          e);
    }
  }

  // --- object basics --------------------------------------------------------

}
