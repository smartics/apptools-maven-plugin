/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.tools.file.FileUtilities;
import de.smartics.maven.apptools.tools.source.AppSourceFolder;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Downloads artifacts from the artifact server.
 *
 * @since 1.0
 */
@Mojo(name = "download", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.COMPILE)
public class DownloadMojo extends AbstractArtifactMojo {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public final class Context extends BaseArtifactContext {

    private Context(final File sourceFolder) {
      super(sourceFolder);
    }

    @Override
    public String toString() {
      return super.toString();
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  protected boolean isDryRunSetOnTheMojo() {
    return false;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping download of artifacts since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      context.logVerbose(
          "Starting download with context:\n" + context + "\n  order: " +
          getOrder());
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      if (isVerbose()) {
        throw new MojoExecutionException(e.getMessage(), e);
      } else {
        throw new MojoExecutionException(e.getMessage());
      }
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    init(false);
    try {
      final File sourceFolder =
          (project == null ? FileUtilities.absolutePathDir(getSourceFolder()) :
           FileUtilities.createFolder(project.getBasedir(), getSourceFolder()));
      return new Context(sourceFolder);
    } catch (final FileNotFoundException e) {
      throw new AppException("Cannot find apps folder: " + e.getMessage(), e);
    }
  }

  private void run(final Context context) {
    final AppSourceFolder appSource = createAppSource(context);
    context.logInfo("Download completed successfully!\n" + appSource);
  }

  // --- object basics --------------------------------------------------------

}
