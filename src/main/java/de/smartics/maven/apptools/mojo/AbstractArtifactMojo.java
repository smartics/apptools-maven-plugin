/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.ArtifactSpecification;
import de.smartics.maven.apptools.domain.FileAcceptorContext;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.tools.file.Acceptor;
import de.smartics.maven.apptools.tools.file.FileUtilities;
import de.smartics.maven.apptools.tools.maven.ArtifactDownloadHelper;
import de.smartics.maven.apptools.tools.source.AppSourceFolder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.handler.manager.ArtifactHandlerManager;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.artifact.ArtifactCoordinate;
import org.apache.maven.shared.artifact.resolve.ArtifactResolver;
import org.apache.maven.shared.repository.RepositoryManager;
import org.codehaus.mojo.versions.api.ArtifactVersions;
import org.codehaus.mojo.versions.api.DefaultVersionsHelper;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Base implementation for mojos accessing artifacts on the artifact server.
 */
public abstract class AbstractArtifactMojo extends AbstractVersionUpmMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private static final String SNAPSHOT = "SNAPSHOT";
  private static final String RELEASE = "RELEASE";

  /**
   * The folder with application files to process.
   * <p>
   * User property: <tt>apptools.source</tt>, <tt>source</tt>
   * </p>
   * <p>
   * Defaults to '<tt>target/apps</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String sourceFolder;

  /**
   * List of remote artifact repositories.
   */
  @Parameter(defaultValue = "${project.remoteArtifactRepositories}",
      readonly = true, required = true)
  protected List<ArtifactRepository> remoteRepositories;

  @Component
  protected ArtifactResolver artifactResolver;

  @Component
  protected RepositoryManager repositoryManager;

  @Component
  protected ArtifactHandlerManager artifactHandlerManager;

  /**
   * List of specification strings to locate artifacts on the artifact server
   * and download them to the configured source repository.
   * <p>
   * User property: <tt>apptools.artifacts</tt>, <tt>artifacts</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> artifacts;

  /**
   * Override any specified versions in the POM with the latest version. Use
   * <code>SNAPSHOT</code> for the latest snapshot release,
   * <code>RELEASE</code>
   * for the latest public release.
   * <p>
   * User property: <tt>apptools.useLatest</tt>, <tt>useLatest<tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String useLatest;

  /**
   * If the source folder is not empty, the build will be terminated with an
   * fail message. This prevents the user to deploy artifacts in an existing
   * folder.
   * <p>
   * User property: <tt>apptools.ignoreNonEmptySourceFolder</tt>,
   * <tt>ignoreNonEmptySourceFolder</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean ignoreNonEmptySourceFolder;


  /**
   * Strings to match in app file names for inclusion. If empty, everything is
   * included. Suffix searches are supported by appending the dollar character
   * ($) to the include string.
   * <p>
   * User property: <tt>apptools.includes</tt>, <tt>includes</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> includes;

  /**
   * Strings to match in app file names for exclusion. If empty, nothing is
   * excluded. Suffix searches are supported by appending the dollar character
   * ($) to the include string.
   * <p>
   * User property: <tt>apptools.excludes</tt>, <tt>excludes</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> excludes;

  /**
   * Specify the file name extensions to accept.
   * <p>
   * User property: <tt>apptools.acceptedFilenameExtensions</tt>,
   * <tt>acceptedFilenameExtensions</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> acceptedFilenameExtensions;

  /**
   * Allows to impose a sort order on the list of artifacts to be deployed. The
   * list represents stages (1, 2, 3, ...) that will deploy all matching
   * artifacts.
   * <p>
   * User property: <tt>apptools.order</tt>, <tt>order</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> order;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public abstract class BaseArtifactContext extends VersionUpmContext
      implements FileAcceptorContext {
    private final File sourceFolder;
    private final Acceptor acceptor;

    protected BaseArtifactContext(final File sourceFolder) {
      this.sourceFolder = sourceFolder;
      acceptor = Acceptor.create(getIncludes(), getExcludes());
    }

    @Override
    public File getSourceFolder() {
      return sourceFolder;
    }

    @Override
    public boolean isAccepted(final File file) {
      final List<String> acceptedFilenameExtensions =
          getAcceptedFilenameExtensions();
      if (acceptedFilenameExtensions != null &&
          !acceptedFilenameExtensions.isEmpty()) {
        final String extension = FilenameUtils.getExtension(file.getName());
        if (!acceptedFilenameExtensions.contains(extension)) {
          return false;
        }
      }
      return acceptor.isAccepted(file);
    }

    public boolean isAccepted(final String artifactId) {
      return acceptor.isAccepted(artifactId);
    }

    public boolean isAccepted(final Plugin plugin) {
      return isAccepted(plugin.getKey());
    }

    public boolean useLatest() {
      final String useLatest = getUseLatest();
      return RELEASE.equals(useLatest) || SNAPSHOT.equals(useLatest);
    }

    @Override
    public String toString() {
      return "  source: " + sourceFolder.getAbsolutePath() + "\n  " + acceptor;
    }

    @Override
    public DateFormat getQualifierDateFormat() {
      return null;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getSourceFolder() {
    return getUserPropertyAsString("sourceFolder", sourceFolder, "target/apps");
  }

  public List<String> getArtifacts() {
    return getUserPropertyAsList("artifacts", artifacts);
  }

  public String getUseLatest() {
    return getUserPropertyAsString("useLatest", useLatest);
  }

  protected boolean isIgnoreNonEmptySourceFolder() {
    return isUserPropertySet("ignoreNonEmptySourceFolder") ||
           ignoreNonEmptySourceFolder;
  }

  public List<String> getIncludes() {
    return getUserPropertyAsList("includes", includes);
  }

  public List<String> getExcludes() {
    return getUserPropertyAsList("excludes", excludes);
  }

  public List<String> getAcceptedFilenameExtensions() {
    return getUserPropertyAsList("acceptedFilenameExtensions",
        acceptedFilenameExtensions);
  }

  public List<String> getOrder() {
    return getUserPropertyAsList("order", order);
  }

  // --- business -------------------------------------------------------------

  protected AppSourceFolder createAppSource(final BaseArtifactContext context) {
    final List<String> artifacts = getArtifacts();
    if (artifacts != null && !artifacts.isEmpty()) {
      return provideArtifacts(context, artifacts);
    } else {
      return AppSourceFolder.create(context, context.getSourceFolder());
    }
  }

  private AppSourceFolder provideArtifacts(final BaseArtifactContext context,
      final List<String> artifactSpecificationStrings) {
    final File sourceFolder = context.getSourceFolder();
    if (!FileUtilities.isEmpty(sourceFolder)) {
      if (!isIgnoreNonEmptySourceFolder()) {
        throw new AppException("Folder '" + sourceFolder.getAbsolutePath() +
                               " is not empty. Clear the folder or set" +
                               " -DignoreNonEmptySourceFolder=true to reuse " +
                               "the artifacts in" +
                               " this folder.");
      }
      context.logVerbose("Folder '" + sourceFolder.getAbsolutePath() +
                         "' is NOT EMPTY. Downloading additional artifacts to" +
                         " this folder.");
    }
    final List<ArtifactSpecification> artifactSpecifications =
        parseArtifactSpecifications(artifactSpecificationStrings);
    final ArtifactDownloadHelper artifactDownloadHelper =
        new ArtifactDownloadHelper(mavenSession, getLog(), artifactResolver,
            remoteRepositories);
    for (final ArtifactSpecification artifactSpecification :
        artifactSpecifications) {
      if (context.isAccepted(artifactSpecification.getArtifactId())) {
        final ArtifactCoordinate coordinate =
            calcArtifactCoordinate(context, artifactSpecification);
        final Artifact artifact = artifactDownloadHelper.download(coordinate);
        if (artifact == null) {
          throw new AppException(
              "Cannot download artifact '" + artifactSpecification + "'.");
        }
        final File file = artifact.getFile();
        try {
          FileUtils.copyFileToDirectory(file, context.getSourceFolder());
          context.logVerbose(
              "  " + artifactSpecification + " -> " + file.getAbsolutePath());
        } catch (final IOException e) {
          throw new AppException(
              "Cannot copy file '" + file.getAbsolutePath() + "' to '" +
              sourceFolder.getAbsolutePath() + ": " + e.getMessage(), e);
        }
      } else {
        context.logVerbose("  SKIPPING: " + artifactSpecification +
                           " since it is rejected due to includes/excludes.");
      }
    }
    return AppSourceFolder.create(context, sourceFolder);
  }

  private List<ArtifactSpecification> parseArtifactSpecifications(
      final List<String> artifactSpecificationStrings) {
    final List<ArtifactSpecification> artifactSpecifications =
        new ArrayList<>(artifactSpecificationStrings.size());
    for (final String specificationString : artifactSpecificationStrings) {
      final ArtifactSpecification specification =
          ArtifactSpecification.fromString(specificationString);
      artifactSpecifications.add(specification);
    }
    return artifactSpecifications;
  }

  private ArtifactCoordinate calcArtifactCoordinate(
      final BaseArtifactContext context,
      final ArtifactSpecification artifactSpecification) {
    final ArtifactCoordinate coordinate;
    if (context.useLatest()) {
      final ArtifactVersion version =
          fetchLatestVersion(context, artifactSpecification);
      coordinate = artifactSpecification.createArtifactCoordinate(
          version != null ? version.toString() : "LATEST");
    } else {
      coordinate = artifactSpecification.createArtifactCoordinate(null);
    }
    return coordinate;
  }

  private ArtifactVersion fetchLatestVersion(final BaseArtifactContext context,
      final ArtifactSpecification artifactSpecification) {
    try {
      final DefaultVersionsHelper helper = context.getVersionsHelper();
      final ArtifactVersions versions = helper.lookupArtifactVersions(
          artifactSpecification.createArtifact(artifactHandler), false);
      versions.setIncludeSnapshots(SNAPSHOT.equals(getUseLatest()));
      return versions.getNewestVersion(null, null);
    } catch (final Exception e) {
      context.logWarn(
          "Cannot fetch latest version for artifact '" + artifactSpecification +
          "', using default: " + e.getMessage());
      return null;
    }
  }

  protected static boolean isNotEmpty(final List<?> list) {
    return !(list == null || list.isEmpty());
  }

  // --- object basics --------------------------------------------------------

}
