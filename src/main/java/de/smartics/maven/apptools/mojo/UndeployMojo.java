/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import static de.smartics.maven.apptools.domain.PluginAcceptorContext.MODE_LOCAL;
import static de.smartics.maven.apptools.domain.PluginAcceptorContext.isLocal;
import static de.smartics.maven.apptools.domain.PluginAcceptorContext.isRemote;

import de.smartics.maven.apptools.domain.AccessPlugins;
import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.AppSource;
import de.smartics.maven.apptools.domain.PluginAcceptorContext;
import de.smartics.maven.apptools.domain.UndeployTarget;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.resources.Plugins;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestCheckDeployed;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestUndeployer;
import de.smartics.maven.apptools.tools.file.BucketLine;
import de.smartics.maven.apptools.tools.file.FileUtilities;
import de.smartics.maven.apptools.tools.source.AppSourceFolder;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Remove deployed apps from the Confluence server.
 *
 * @since 1.0
 */
@Mojo(name = "undeploy", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class UndeployMojo extends AbstractArtifactMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * A simple flag to run on the specified environment, but without actually
   * undeploying apps on the target.
   * <p>
   * User property: <tt>apptools.dryRun</tt>, <tt>dryRun</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean dryRun;

  /**
   * The path to the service on the server.
   * <p>
   * User property: <tt>atlassian.pluginServicePath</tt>,
   * <tt>pluginServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/${plugin.key}-key</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String pluginServicePath;

  /**
   * The path to the service on the server to check which apps are deployed.
   * <p>
   * User property: <tt>atlassian.deployedAppsServicePath</tt>,
   * <tt>deployedAppsServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/monitor/installed</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String deployedAppsServicePath;

  /**
   * List of key for plugins to remove.
   * <p>
   * User property: <tt>apptools.pluginKeys</tt>, <tt>pluginKeys</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> pluginKeys;

  /**
   * Specify whether to extract keys from the local artifacts
   * (<code>LOCAL</code>), fetch keys from the remote server
   * (<code>REMOTE</code>) or both (<code>BOTH</code>). Note that
   * <code>includes</code> and <code>excludes</code> are applied in either case.
   * <p>
   * User property: <tt>apptools.mode</tt>, <tt>mode</tt>
   * </p>
   * <p>
   * Defaults to '<tt>LOCAL</tt>'.
   *
   * @since 1.0
   */
  @Parameter
  private String mode;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public final class Context extends BaseArtifactContext
      implements PluginAcceptorContext {
    private Context(final File sourceFolder) {
      super(sourceFolder);
    }

    @Override
    public boolean isAccepted(final Plugin plugin) {
      return super.isAccepted(plugin.getKey());
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected boolean isDryRunSetOnTheMojo() {
    return dryRun;
  }

  public String getPluginsServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.", "pluginServicePath",
        pluginServicePath, "/rest/plugins/1.0/${plugin.key}-key");
  }

  public String getDeployedAppsServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "deployedAppsServicePath", deployedAppsServicePath,
        "/rest/plugins/1.0/monitor/installed");
  }

  public List<String> getPluginKeys() {
    return getUserPropertyAsList("pluginKeys", pluginKeys);
  }

  public String getMode() {
    return getUserPropertyAsString("mode", mode, MODE_LOCAL);
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping undeploying apps since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      throw new MojoExecutionException(e.getMessage());
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    init();

    try {
      final String sourceFolderString = getSourceFolder();
      final File sourceFolder = sourceFolderString != null
          ? FileUtilities.createFolder(project.getBasedir(), sourceFolderString)
          : null;
      return new Context(sourceFolder);
    } catch (final FileNotFoundException e) {
      throw new AppException("Cannot find apps folder.", e);
    }
  }

  @SuppressFBWarnings("REC_CATCH_EXCEPTION")
  private void run(final Context context)
      throws MojoExecutionException, MojoFailureException {
    try (final UndeployTarget target = ConfluenceRestUndeployer.a(context)
        .username(getUsername())
        .password(getPassword())
        .serverUrl(getServerUrl())
        .tokenServicePath(getTokenServicePath())
        .servicePath(getPluginsServicePath())
        .timeoutMs(getTimeoutMs())
        .build()) {
      for (final String pluginKey : getPluginKeys()) {
        target.undeploy(pluginKey);
      }

      final String mode = getMode();
      if (isLocal(mode) && context.getSourceFolder() != null) {
        final AppSource appSource =
            AppSourceFolder.create(context, context.getSourceFolder());
        final BucketLine line = new BucketLine(getOrder(), appSource);
        for (final App app : line) {
          final String key = app.getKey();
          target.undeploy(key);
        }
      }

      if (isRemote(mode)
          && (isNotEmpty(getIncludes()) || isNotEmpty(getExcludes()))) {
        try (final AccessPlugins access = ConfluenceRestCheckDeployed.a(context)
            .username(getUsername())
            .password(getPassword())
            .serverUrl(getServerUrl())
            .tokenServicePath(getTokenServicePath())
            .servicePath(getDeployedAppsServicePath())
            .timeoutMs(getTimeoutMs())
            .build()) {
          final Plugins plugins = access.fetchPlugins();
          for (final Plugin plugin : plugins) {
            final String pluginKey = plugin.getKey();
            target.undeploy(pluginKey);
          }
        } catch (final Exception e) {
          throw new AppException(
              "Failed to undeploy apps from remote server: " + e.getMessage(),
              e);
        }
      }
    } catch (final Exception e) {
      throw new AppException(
          "Failed undeploy from remote server: " + e.getMessage(), e);
    }
  }

  // --- object basics --------------------------------------------------------

}
