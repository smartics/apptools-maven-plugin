/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.tools.file.StringUtilities;
import de.smartics.maven.apptools.tools.security.SettingsDecrypter;
import org.apache.maven.model.Profile;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.util.StringUtils;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import java.util.List;

/**
 * Base implementation for all mojos accessing the UPM in this project.
 *
 * @since 1.0
 */
public abstract class AbstractUpmMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Helper to decrypt passwords from the settings.
   *
   * @since 1.0
   */
  @Component(role =
      org.sonatype.plexus.components.sec.dispatcher.SecDispatcher.class)
  private SecDispatcher securityDispatcher;

  /**
   * The location of the <code>settings-security.xml</code>.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${user.home}/.m2/settings-security.xml")
  private String settingsSecurityLocation;

  /**
   * The name of the user to authenticate to the remote server.
   * <p>
   * User property: <tt>apptools.username</tt>, <tt>username</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String username;

  /**
   * The password of the user to authenticate to the remote server.
   * <p>
   * User property: <tt>apptools.password</tt>, <tt>password</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String password;

  /**
   * The identifier for the server to fetch credentials in case
   * <tt>username</tt> or <tt>password</tt> are not set explicitly. The
   * credentials are used to connect to the REST API of the remote server.
   * <p>
   * User property: <tt>apptools.serverId</tt>, <tt>serverId</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String serverId;

  /**
   * The base server URL to locate services. This value is required, and is
   * probably set via a profile in case more than one environment is targeted.
   * Per convention, profiles containing a environment specific configuration,
   * do not contain lower case letters.
   * <p>
   * User property: <tt>apptools.serverUrl</tt>, <tt>serverUrl</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String serverUrl;

  /**
   * The path to a service to fetch a token on the server.
   * <p>
   * User property: <tt>atlassian.tokenServicePath</tt>,
   * <tt>tokenServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String tokenServicePath;

  /**
   * The timeout in milliseconds for any connection issues accessing the remote
   * service.
   * <p>
   * User property: <tt>apptools.timeout</tt>, <tt>timeout</tt>
   * </p>
   * <p>
   * Defaults to '<tt>5000</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private Integer timeoutMs;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public class BaseUpmContext extends BaseContext {
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getUsername() {
    return getUserPropertyAsString("username", username);
  }

  public String getPassword() {
    return getUserPropertyAsString("password", password);
  }

  public String getServerId() {
    return getUserPropertyAsString("serverId", serverId);
  }

  public String getServerUrl() {
    return getUserPropertyAsString("serverUrl", serverUrl);
  }

  public String getTokenServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.", "tokenServicePath",
        tokenServicePath, "/rest/plugins/1.0/");
  }

  public int getTimeoutMs() {
    return getUserPropertyAsInteger("timeoutMs", timeoutMs, 5000);
  }

  // --- business -------------------------------------------------------------

  protected void init() throws AppException {
    init(true);
  }

  protected void init(final boolean requiresUpmAccess) throws AppException {
    if (requiresUpmAccess) {
      checkServerUrlProvided();

      final String username = getUsername();
      final String password = getPassword();
      final String serverId = getServerId();
      final boolean hasUsername = StringUtils.isNotBlank(username);
      final boolean hasPassword = StringUtils.isNotBlank(password);
      if (StringUtils.isNotBlank(serverId)) {
        if (!(hasUsername && hasPassword)) {
          provideCredentialsFromServerConfig(hasUsername, hasPassword);
        }
      } else {
        if (!(hasUsername && hasPassword)) {
          throw new AppException(
              "No server configuration provided and credentials are " +
              "incomplete:" +
              " username=" + (hasUsername ? username : "<none>") +
              ", password=<" + (hasPassword ? "provided>" : "missing>") +
              "Please provide username and passwort on the commandline or" +
              " a server configuration. See" +
              " https://maven.apache.org/settings.html#Servers for details" +
              " on how to provide a server configuration.");
        }
      }
    }
  }

  private void checkServerUrlProvided() {
    final String serverUrl = getServerUrl();
    if (StringUtils.isBlank(serverUrl)) {
      if (project != null) {
        final StringBuilder buffer = new StringBuilder(512);
        buffer.append(
            "Cannot find a specified 'serverUrl'. By default we consider" +
            " profiles with IDs that contain no lowercase characters as" +
            " environment profiles.");
        final List<Profile> activeEnvProfiles = calcActiveEnvironmentProfiles();
        if (!activeEnvProfiles.isEmpty()) {
          buffer.append(
              "\nCurrently the following environment profiles are activated:");
          for (final Profile profile : activeEnvProfiles) {
            buffer.append(' ').append(profile.getId()).append(" (")
                .append(profile.getSource()).append(')');
          }
          buffer.append(
              "\nUnfortunately none of them provides the 'serverUrl' property" +
              ".");
        }

        buffer.append(
            "\nPlease make sure that one of the following profiles provides a" +
            " 'serverUrl' and is active:");
        for (final Profile profile : project.getModel().getProfiles()) {
          final String id = profile.getId();
          if (StringUtilities.hasNoLowerCase(id)) {
            buffer.append(' ').append(id).append(" (")
                .append(profile.getSource()).append(')');
          }
        }
        buffer.append(
            "\nPlease refer to https://www.smartics.eu/confluence/x/IgCiBg" +
            " for more information on using Maven build profiles to" +
            " configure environments.");
        throw new AppException(buffer.toString());
      } else {
        throw new AppException(
            "No 'serverUrl' specified. Please specify one with -DserverUrl.");
      }
    }
  }

  private void provideCredentialsFromServerConfig(final boolean hasUsername,
      final boolean hasPassword) throws AppException {
    final Server server = fetchServer();
    if (!hasUsername) {
      username = server.getUsername();
    }
    if (!hasPassword) {
      final String plain = server.getPassword();
      try {
        final SettingsDecrypter settingsDecrypter =
            new SettingsDecrypter(securityDispatcher, settingsSecurityLocation);
        password = settingsDecrypter.decrypt(plain);
      } catch (final SecDispatcherException e) {
        final String serverId = getServerId();
        getLog().warn(
            "Cannot decrypt password for server '" + serverId + "': " +
            e.getMessage() +
            " -> Using plain password which will work if the password" +
            " is actually not encrypted. See" +
            " https://maven.apache.org/guides/mini/guide-encryption.html" +
            " for details.");
        password = plain;
      }
    }
  }

  private Server fetchServer() throws AppException {
    final Settings settings = mavenSession.getSettings();
    final String serverId = getServerId();
    if (settings == null) {
      throw new AppException(
          "No settings provided to find server configuration '" + serverId +
          "'. Cannot provide credentials. See" +
          " https://maven.apache.org/settings.html for details.");
    }
    final Server server = settings.getServer(serverId);
    if (server == null) {
      throw new AppException("No server configuration found for '" + serverId +
                             "'. Cannot provide credentials. See" +
                             " https://maven.apache.org/settings.html#Servers" +
                             " for details.");
    }
    return server;
  }

  // --- object basics --------------------------------------------------------

}
