/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.ArtifactSpecification;
import de.smartics.maven.apptools.tools.maven.VersionHelper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginManagement;
import org.apache.maven.model.Profile;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.path.PathTranslator;
import org.apache.maven.repository.legacy.metadata.ArtifactMetadataRetrievalException;
import org.apache.maven.settings.Settings;
import org.apache.maven.shared.utils.StringUtils;
import org.codehaus.mojo.versions.api.ArtifactVersions;
import org.codehaus.mojo.versions.api.DefaultVersionsHelper;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Check if there are newer versions of apps than those specified in the
 * configuration.
 * <p>
 * Compare the configured artifacts with those on the artifact server. Note that
 * two snapshots of the same version are considered to be the same.
 * </p>
 *
 * @since 1.0
 */
@SuppressWarnings("deprecation")
@Mojo(name = "check-configuration", threadSafe = true, requiresProject = true,
    defaultPhase = LifecyclePhase.NONE)
public class CheckConfigurationMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Component
  private org.apache.maven.artifact.factory.ArtifactFactory artifactFactory;

  @Component
  private org.apache.maven.artifact.resolver.ArtifactResolver resolver;

  @Component
  private MavenProjectBuilder projectBuilder;

  @Parameter(defaultValue = "${reactorProjects}", required = true,
      readonly = true)
  private List<MavenProject> reactorProjects;

  @Component
  private ArtifactMetadataSource artifactMetadataSource;

  @Parameter(defaultValue = "${project.remoteArtifactRepositories}",
      readonly = true)
  private List<ArtifactRepository> remoteArtifactRepositories;

  @Parameter(defaultValue = "${project.pluginArtifactRepositories}",
      readonly = true)
  private List<ArtifactRepository> remotePluginRepositories;

  @Parameter(defaultValue = "${localRepository}", readonly = true)
  private ArtifactRepository localRepository;

  @Component
  private WagonManager wagonManager;

  @Parameter(defaultValue = "${settings}", readonly = true)
  private Settings settings;

  /**
   * URI of a ruleSet file containing the rules that control how to compare
   * version numbers. The URI could be either a Wagon URI or a classpath URI
   * (e.g. <code>classpath:///package/sub/package/rules.xml</code>).
   *
   * @since 1.0
   */
  @Parameter(property = "rulesUri")
  private String rulesUri;

  @Component
  private PathTranslator pathTranslator;

  @Component
  private ArtifactResolver artifactResolver;

  @Component
  private ArtifactHandler artifactHandler;

  /**
   * The server ID to lookup additional credentials in the <tt>settings.xml<tt>.
   * This is used when wagon needs extra authentication information.
   * <p>
   * User property: <tt>apptools.artifactServerId</tt>,
   * <tt>artifactServerId</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String artifactServerId;

  /**
   * If set to <code>true</code> only releases are considered. Otherwise
   * snapshots are also taken into account.
   * <p>
   * User property: <tt>apptools.requireReleases</tt>, <tt>requireReleases</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean requireReleases;

  /**
   * List of specification strings to locate artifacts on the artifact server.
   * <p>
   * User property: <tt>apptools.artifacts</tt>, <tt>artifacts</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> artifacts;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private final class VersionResolver {
    private String resolveVersion(final String plainVersionString) {
      final String trimmedVersion = plainVersionString.trim();
      final int trimmedVersionLength = trimmedVersion.length();
      if (trimmedVersion.startsWith("${")
          && trimmedVersion.charAt(trimmedVersionLength - 1) == '}') {
        final String versionRef =
            trimmedVersion.substring(2, trimmedVersionLength - 1);
        final String version = getProperty(versionRef);
        return version;
      }
      return plainVersionString;
    }

    private String getProperty(final String key) {
      String value;
      final Properties properties = project.getProperties();
      if (properties != null) {
        value = properties.getProperty(key);
        if (StringUtils.isBlank(value)) {
          for (final Profile profile : project.getActiveProfiles()) {
            final Properties profileProperties = profile.getProperties();
            if (profileProperties != null) {
              value = profileProperties.getProperty(key);
              if (StringUtils.isNotBlank(value)) {
                return value;
              }
            }
          }
        }
      }

      return null;
    }
  }

  // ****************************** Inner Classes *****************************

  public final class Context extends BaseContext {
    private final DefaultVersionsHelper helper;

    private Context() throws AppException {
      try {
        helper = new DefaultVersionsHelper(artifactFactory, artifactResolver,
            artifactMetadataSource, remoteArtifactRepositories,
            remotePluginRepositories, localRepository, wagonManager, settings,
            getArtifactServerId(), rulesUri, getLog(), mavenSession,
            pathTranslator);
      } catch (final MojoExecutionException e) {
        throw new AppException("Cannot create versions environment.", e);
      }
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected boolean isDryRunSetOnTheMojo() {
    return false;
  }

  public String getArtifactServerId() {
    return getUserPropertyAsString("artifactServerId", artifactServerId);
  }

  public boolean isRequireReleases() {
    return isUserPropertySet("requireReleases") || requireReleases;
  }

  public List<String> getArtifacts() {
    return getUserPropertyAsList("artifacts", artifacts);
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping check for latest apps since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      throw new MojoExecutionException(e.getMessage());
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    init();
    return new Context();
  }

  private void run(final Context context)
      throws MojoExecutionException, MojoFailureException {
    final List<Artifact> artifacts = fetchAppArtifacts();
    final String format = calcFormat(artifacts);
    final StringBuilder buffer = new StringBuilder(4096);
    final String legend = String.format(format, "Artifact ID",
        "Current Artifact Version", "Latest Artifact Version");
    buffer.append('\n')
        .append(legend);
    for (final Artifact artifact : artifacts) {
      try {
        final ArtifactVersions versions = context.helper
            .lookupArtifactUpdates(artifact, !isRequireReleases(), false);
        final ArtifactVersion version =
            VersionHelper.getNewestUpdate(artifact, versions);
        final String line = String.format(format, artifact.getArtifactId(),
            artifact.getVersion(), version != null ? version : "IS LATEST!");
        buffer.append('\n')
            .append(line);
      } catch (final ArtifactMetadataRetrievalException
          | OverConstrainedVersionException e) {
        final String line = String.format(format, artifact.getArtifactId(),
            artifact.getVersion(), ">FAILED<: " + e.getMessage());
        buffer.append('\n')
            .append(line);
      }
    }

    getLog().info("Configured artifacts in the POM for environment '"
        + context.getActiveEnvironmentProfile()
        + "' compared to artifacts on the artifact server:" + buffer);
  }

  private String calcFormat(final List<Artifact> artifacts) {
    int maxKey = 0;
    for (final Artifact artifact : artifacts) {
      final String key = artifact.getArtifactId();
      if (key != null) {
        maxKey = Math.max(key.length(), maxKey);
      }
    }

    return "%" + (maxKey + 1) + "s  %-25s -> %25s";
  }

  @SuppressFBWarnings("RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE")
  private List<Artifact> fetchAppArtifacts() {
    final List<Artifact> artifacts = new ArrayList<>();
    if (getArtifacts() != null) {
      fetchArtifactsFromPluginConfiguration(artifacts);
    } else {
      fetchArtifactsFromDependencyPlugin(artifacts);
    }

    return artifacts;
  }

  private void fetchArtifactsFromPluginConfiguration(
      final List<Artifact> artifacts) {
    for (final String artifactSpecificationString : getArtifacts()) {
      final ArtifactSpecification artifactSpecification =
          ArtifactSpecification.fromString(artifactSpecificationString);
      final Artifact artifact =
          artifactSpecification.createArtifact(artifactHandler);
      artifacts.add(artifact);
    }
  }

  private void fetchArtifactsFromDependencyPlugin(
      final List<Artifact> artifacts) {
    final PluginManagement pluginManagement = project.getPluginManagement();
    if (pluginManagement != null) {
      final Plugin dependencyPlugin =
          fetchPlugin("org.apache.maven.plugins:maven-dependency-plugin",
              pluginManagement.getPlugins());
      if (dependencyPlugin != null) {
        extractArtifactsFromPlugin(artifacts, dependencyPlugin);
      }
    }
    final Plugin dependencyPlugin =
        fetchPlugin("org.apache.maven.plugins:maven-dependency-plugin",
            project.getBuildPlugins());
    if (dependencyPlugin != null) {
      extractArtifactsFromPlugin(artifacts, dependencyPlugin);
    }
  }

  private Plugin fetchPlugin(final String pluginKey,
      final List<Plugin> plugins) {
    for (final Plugin plugin : plugins) {
      if (pluginKey.equals(plugin.getKey())) {
        return plugin;
      }
    }
    return null;
  }

  private void extractArtifactsFromPlugin(final List<Artifact> artifacts,
      final Plugin plugin) {
    final Object configObject = plugin.getConfiguration();
    if (configObject instanceof Xpp3Dom) {
      final Xpp3Dom configuration = (Xpp3Dom) configObject;
      final Xpp3Dom artifactItemsElement =
          configuration.getChild("artifactItems");
      if (artifactItemsElement != null) {
        final Xpp3Dom[] artifactItemElements =
            artifactItemsElement.getChildren("artifactItem");
        if (artifactItemElements != null) {
          final VersionResolver resolver = new VersionResolver();
          for (final Xpp3Dom artifactItemElement : artifactItemElements) {
            final String groupId =
                getValue(artifactItemElement.getChild("groupId"));
            final String artifactId =
                getValue(artifactItemElement.getChild("artifactId"));
            final String version = resolver.resolveVersion(
                getValue(artifactItemElement.getChild("version"), ""));
            if (version == null) {
              throw new AppException("No version found for artifact " + groupId
                  + ':' + artifactId + '.');
            }
            final String type =
                getValue(artifactItemElement.getChild("type"), "jar");
            final Artifact artifact = new DefaultArtifact(groupId, artifactId,
                version, "compile", type, null, artifactHandler);
            artifacts.add(artifact);
          }
        }
      }
    }
  }

  private static String getValue(final Xpp3Dom element) {
    return getValue(element, null);
  }

  private static String getValue(final Xpp3Dom element,
      final String defaultValue) {
    if (element != null) {
      return element.getValue();
    }
    return defaultValue;
  }

  // --- object basics --------------------------------------------------------

}
