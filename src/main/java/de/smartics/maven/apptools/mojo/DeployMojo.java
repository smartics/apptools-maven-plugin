/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AccessPlugins;
import de.smartics.maven.apptools.domain.App;
import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.AppSource;
import de.smartics.maven.apptools.domain.DeployTarget;
import de.smartics.maven.apptools.domain.FileAcceptorContext;
import de.smartics.maven.apptools.resources.Plugin;
import de.smartics.maven.apptools.resources.PluginMetadata;
import de.smartics.maven.apptools.resources.Plugins;
import de.smartics.maven.apptools.resources.Plugins.PluginMap;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestAppMetadata;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestCheckDeployed;
import de.smartics.maven.apptools.tools.deploy.ConfluenceRestDeployer;
import de.smartics.maven.apptools.tools.enable.ConfluenceRestEnablementChecker;
import de.smartics.maven.apptools.tools.file.Acceptor;
import de.smartics.maven.apptools.tools.file.BucketLine;
import de.smartics.maven.apptools.tools.file.FileUtilities;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Deploy apps to the Confluence server.
 *
 * @since 1.0
 */
@Mojo(name = "deploy", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.COMPILE)
public class DeployMojo extends AbstractArtifactMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * A simple flag to run on the specified environment, but without actually
   * deploying apps on the target.
   * <p>
   * User property: <tt>apptools.dryRun</tt>, <tt>dryRun</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean dryRun;

  /**
   * The path to the service on the server.
   * <p>
   * User property: <tt>apptools.pluginsServicePath</tt>,
   * <tt>pluginsServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  protected String pluginsServicePath;

  /**
   * The path to the checker service to check the status of a deployed app on
   * the server.
   * <p>
   * User property: <tt>atlassian.enablementStateServicePath</tt>,
   * <tt>enablementStateServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/${plugin.key}-key/summary</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  protected String enablementStateServicePath;

  /**
   * The path to the service on the server to fetch information about apps
   * deployed on the server.
   * <p>
   * User property: <tt>atlassian.deployedAppsServicePath</tt>,
   * <tt>deployedAppsServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/monitor/installed</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String deployedAppsServicePath;

  /**
   * The path to the service on the server to fetch information about apps
   * deployed on the server.
   * <p>
   * User property: <tt>atlassian.deployedAppsMetadataServicePath</tt>,
   * <tt>deployedAppsMetadataServicePath</tt>
   * </p>
   * <p>
   * Defaults to '<tt>/rest/plugins/1.0/available/${plugin.key}-key</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String deployedAppsMetadataServicePath;

  /**
   * Specify the apps that need to be checked to be successfully enabled.
   * <p>
   * User property: <tt>apptools.check</tt>, <tt>check</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private List<String> checkEnablement;

  /**
   * Force the deployment of apps even if they are not newer than their deployed
   * versions.
   * <p>
   * The deployment time returned form the application server is at day
   * precision. Therefore all artifacts with the same snapshot version that are
   * created on the same day of the last release are the same. To override you
   * need to set this parameter to <code>true</code>.
   * </p>
   * <p>
   * User property: <tt>apptools.forceDeploy</tt>, <tt>forceDeploy</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private boolean forceDeploy;

  /**
   * Specify the time the enablement check should wait if failed.
   * <p>
   * Defaults to '3000'.
   * </p>
   * <p>
   * User property: <tt>apptools.checkWait</tt>, <tt>checkWait</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private Long checkWaitMs;

  /**
   * Specify the number of times should be conducted. The total wait for a
   * finally failed enablement check would be checkWaitMs times checkMaxCount
   * milliseconds.
   * <p>
   * User property: <tt>apptools.checkMaxCount</tt>, <tt>checkMaxCount</tt>
   * </p>
   * <p>
   * Defaults to '5'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private Integer checkMaxCount;

  /**
   * The pattern used to parse qualifier from the version reported by the
   * deployed plugin. If the qualifier does match, the versions of the deployed
   * plugin and the local apps are compared based on this date information.
   * <p>
   * User property: <tt>apptools.qualifierDatePattern</tt>,
   * <tt>qualifierDatePattern</tt>
   * </p>
   * <p>
   * Defaults to 'yyyyMMdd.HHmmss'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String qualifierDatePattern;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public final class Context extends BaseArtifactContext
      implements FileAcceptorContext {
    private final Acceptor enablementAcceptor;

    private Context(final File sourceFolder) {
      super(sourceFolder);
      enablementAcceptor = Acceptor.create(getCheckEnablement(), null);
    }

    public boolean enablementCheckRequired(final String name) {
      return enablementAcceptor.isAccepted(name);
    }

    public long getCheckWaitMs() {
      return DeployMojo.this.getCheckWaitMs();
    }

    public int getCheckMaxCount() {
      return DeployMojo.this.getCheckMaxCount();
    }

    @Override
    public String toString() {
      return super.toString();
    }

    public String getQualifierDatePattern() {
      return DeployMojo.this.getQualifierDatePattern();
    }

    @Override
    public DateFormat getQualifierDateFormat() {
      return new SimpleDateFormat(getQualifierDatePattern());
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected boolean isDryRunSetOnTheMojo() {
    return dryRun;
  }

  public String getPluginsServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "pluginsServicePath", pluginsServicePath, "/rest/plugins/1.0/");
  }

  public String getCheckerServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "enablementStateServicePath", enablementStateServicePath,
        "/rest/plugins/1.0/${plugin.key}-key/summary");
  }

  public String getDeployedAppsServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "deployedAppsServicePath", deployedAppsServicePath,
        "/rest/plugins/1.0/monitor/installed");
  }

  public String getDeployedAppsMetadataServicePath() {
    return getUserPropertyAsStringByKeyPrefix("atlassian.",
        "deployedAppsMetadataServicePath", deployedAppsMetadataServicePath,
        "/rest/plugins/1.0/available/${plugin.key}-key");
  }

  public List<String> getCheckEnablement() {
    return getUserPropertyAsList("checkEnablement", checkEnablement);
  }

  public long getCheckWaitMs() {
    return getUserPropertyAsLong("checkWaitMs", checkWaitMs, 5000L);
  }

  public int getCheckMaxCount() {
    return getUserPropertyAsInteger("checkWaitMs", checkMaxCount, 5);
  }

  private boolean isForceDeploy() {
    return isUserPropertySet("forceDeploy") || forceDeploy;
  }

  public String getQualifierDatePattern() {
    return getUserPropertyAsString("qualifierDatePattern", qualifierDatePattern,
        "yyyyMMdd.HHmmss");
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final Log log = getLog();
    if (isSkip()) {
      log.info("Skipping deployment of apps since skip='true'.");
      return;
    }

    try {
      final Context context = configure();
      context.logVerbose(
          "Starting deployment with context:\n" + context + "\n  order: " +
          getOrder());
      run(context);
    } catch (final AppException e) {
      log.error("Exiting with error.");
      if (isVerbose()) {
        throw new MojoExecutionException(e.getMessage(), e);
      } else {
        throw new MojoExecutionException(e.getMessage());
      }
    } catch (final RuntimeException e) {
      log.error("Exiting with error.", e);
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private Context configure() throws AppException {
    init();
    try {
      final File sourceFolder =
          (project == null ? FileUtilities.absolutePathDir(getSourceFolder()) :
           FileUtilities.createFolder(project.getBasedir(), getSourceFolder()));
      return new Context(sourceFolder);
    } catch (final FileNotFoundException e) {
      throw new AppException("Cannot find apps folder: " + e.getMessage(), e);
    }
  }

  private void run(final Context context)
      throws MojoExecutionException, MojoFailureException {
    final AppSource appSource = createAppSource(context);
    final boolean forceDeploy = isForceDeploy();
    try (final ConfluenceRestEnablementChecker checker =
        ConfluenceRestEnablementChecker.a(
            context).username(getUsername()).password(getPassword())
        .serverUrl(getServerUrl()).tokenServicePath(getTokenServicePath())
        .servicePath(getCheckerServicePath()).timeoutMs(getTimeoutMs())
        .build()) {
      try (final AccessPlugins access = ConfluenceRestCheckDeployed.a(context)
          .username(getUsername()).password(getPassword())
          .serverUrl(getServerUrl()).tokenServicePath(getTokenServicePath())
          .servicePath(getDeployedAppsServicePath()).timeoutMs(getTimeoutMs())
          .build()) {
        final Plugins plugins = supplyVersion(access.fetchPlugins(), context);
        final PluginMap pluginMap =
            plugins.getPluginMap(context.getQualifierDatePattern());
        try (final DeployTarget target = ConfluenceRestDeployer.a(context)
            .username(getUsername()).password(getPassword())
            .serverUrl(getServerUrl()).tokenServicePath(getTokenServicePath())
            .servicePath(getPluginsServicePath()).timeoutMs(getTimeoutMs())
            .checker(checker).build()) {
          final BucketLine line = new BucketLine(getOrder(), appSource);
          if (line.isEmpty()) {
            context.logWarn("No apps selected for deployment.");
          } else {
            for (final App app : line) {
              final Plugin plugin = pluginMap.getPlugin(app.getKey());
              if (forceDeploy || !pluginMap.isUpToDate(app)) {
                target.deploy(app, plugin);
              } else {
                final Date releaseDate =
                    null != plugin ? plugin.getReleaseDate() : null;
                context.logInfo("App '" + app.getKey() + "' with version '" +
                                app.getVersion() + (plugin != null ?
                                                    "/" + plugin.getVersion() +
                                                    " (local/deployed)" : "") +
                                "' is up-to-date" + (releaseDate != null ?
                                                     " ('" + releaseDate +
                                                     "')" : "") +
                                ". Use -DforceDeploy to override.");
              }
            }
          }
        } catch (final Exception e) {
          throw new AppException(
              "Failed to deploy to remote server: " + e.getMessage(), e);
        }
      } catch (final RuntimeException e) {
        throw new AppException(
            "Failed to fetch information on deployed apps: " + e.getMessage(),
            e);
      }
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      throw new AppException(
          "Failed to enable apps on remote server: " + e.getMessage(), e);
    }
  }

  private Plugins supplyVersion(Plugins plugins, final Context context) {
    try (final ConfluenceRestAppMetadata meta = ConfluenceRestAppMetadata.a(
            context).username(getUsername()).password(getPassword())
        .serverUrl(getServerUrl()).tokenServicePath(getTokenServicePath())
        .servicePath(getDeployedAppsMetadataServicePath())
        .timeoutMs(getTimeoutMs()).build()) {
      for (final Plugin plugin : plugins) {
        final String pluginKey = plugin.getKey();
        final PluginMetadata metadata = meta.metadata(pluginKey);
        if (metadata != null) {
          final Long date = metadata.getReleaseDate();
          plugin.setReleaseDate(date);
        } else {
          context.logVerbose(
              "    No metadata found for version of '" + pluginKey +
              "' on remote server.");
        }
      }

      return plugins;
    } catch (final Exception e) {
      throw new AppException(
          "Failed to supply metadata form remote server: " + e.getMessage(), e);
    }
  }

  // --- object basics --------------------------------------------------------

}
