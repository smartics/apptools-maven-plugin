/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Display usage information on apptools-maven-plugin.
 *
 * @since 1.0
 */
@Mojo(name = "usage", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class UsageMojo extends AbstractMojo {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final StringBuilder buffer = new StringBuilder(512);
    buffer.append("Usage information for the Apptools Maven Plugin")
        .append("\n=== Detailed Descriptions ===")
        .append("\nUse\n   apptools:help \n" + "to list all available goals.")
        .append("\nUse\n   apptools:help -Ddetail -Dgoal=<goal-name>\n"
            + "to fetch more detailed information on a particular goal.");
    buffer.append("\n\n=== General tips ===")
        .append(
            "\n G1. Use Dry Run:\n   If you are unsure what will happen, use -DdryRun.")
        .append(
            "\n G2. Select:\n   Use -Dincludes=aaa,bbb,ccc and -Dexcludes=aaaa,bbbb to select on artifacts or depoyed apps.")
        .append(
            "\n G3. More chatty?\n   Use -Dverbose to get more on INFO level, use -X to get DEBUG information.");
    buffer.append("\n\n=== Tips for App Projects ===")
        .append(
            "\nFind more information online: https://www.smartics.eu/confluence/x/FwCiBg")
        .append(
            "\n A1. Deploy local Artifact:\n   Use apptools:deploy -P<DESTINATION> to deploy the artifact in the target folder to the DESTINATION specified by a profile.")
        .append(
            "\n A2. Check version:\n   Use apptools:check-deployed -P<DESTINATION> to check the deployed version on the DESTINATION specified by a profile.");
    buffer.append("\n\n=== Tips for POM Projects ===")
        .append(
            "\nFind more information online: https://www.smartics.eu/confluence/x/HQCiBg")
        .append(
            "\n P1. Download artifacts locally:\n   The plugin deploys artifacts from a local directory. Use 'mvn clean' to start with an empty folder.")
        .append(
            "\n P2. Check latest versions:\n   Use -DuseLatest=RELEASE or -DuseLatest=SNAPSHOT to override the configured version with the latest release or snapshot version.");
    buffer.append(
        "\n\nFind more information online:\n   https://www.smartics.eu/confluence/x/twF8Bg");

    final Log log = getLog();
    log.info(buffer);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
