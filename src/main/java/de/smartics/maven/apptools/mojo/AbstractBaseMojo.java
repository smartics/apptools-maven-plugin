/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.ExecutionContext;
import de.smartics.maven.apptools.tools.file.StringUtilities;
import de.smartics.maven.apptools.tools.maven.UserPropertiesManager;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Profile;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Base implementation for all mojos in this project.
 *
 * @since 1.0
 */
public abstract class AbstractBaseMojo extends AbstractMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Project for the execution environment.
   *
   * @since 1.0
   */
  @Parameter( defaultValue = "${project}", readonly = true )
  protected MavenProject project;

  /**
   * Session for the execution environment.
   *
   * @since 1.0
   */
  @Parameter( defaultValue = "${session}", readonly = true )
  protected MavenSession mavenSession;

  /**
   * A simple flag to skip the deployment process.
   * <p>
   * User property: <tt>apptools.skip</tt>, <tt>skip</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean skip;

  /**
   * A simple flag to log verbosely.
   * <p>
   * User property: <tt>apptools.verbose</tt>, <tt>verbose</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean verbose;

  private UserPropertiesManager userPropertiesManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public class BaseContext implements ExecutionContext {

    private final Log log;

    private final String activeEnvironmentProfile;

    protected BaseContext() {
      this.log = getLog();
      this.activeEnvironmentProfile = calcActiveEnvironmentProfile();
    }

    private String calcActiveEnvironmentProfile() {
      final List<Profile> profiles = calcActiveEnvironmentProfiles();
      final StringBuilder buffer = new StringBuilder(32);
      for (final Profile profile : profiles) {
        buffer.append(profile.getId())
            .append(' ');
      }

      if (buffer.length() > 0) {
        buffer.setLength(buffer.length() - 1);
      }

      return buffer.toString();
    }

    @Override
    public String getActiveEnvironmentProfile() {
      return activeEnvironmentProfile;
    }

    @Override
    public void logInfo(final String message) {
      log.info(message);
    }

    @Override
    public void logDebug(final String message) {
      log.debug(message);
    }

    @Override
    public boolean isDebug() {
      return log.isDebugEnabled();
    }

    public void logWarn(final String message) {
      log.warn(message);
    }

    @Override
    public void logVerbose(final String message) {
      if (isVerbose()) {
        log.info(message);
      }
    }

    @Override
    public void logVerbose(final Object appSource) {
      if (isVerbose()) {
        log.info(appSource.toString());
      }
    }

    @Override
    public boolean isVerbose() {
      return AbstractBaseMojo.this.isVerbose();
    }

    @Override
    public boolean isDryRun() {
      return AbstractBaseMojo.this.isDryRun();
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected UserPropertiesManager getUserPropertiesManager() {
    if (userPropertiesManager == null) {
      userPropertiesManager = new UserPropertiesManager(mavenSession);
    }
    return userPropertiesManager;
  }

  protected boolean isUserPropertySet(final String name) {
    final UserPropertiesManager manager = getUserPropertiesManager();
    return manager.getPropertyAsBoolean("apptools." + name)
        || manager.getPropertyAsBoolean(name);
  }

  protected String getUserPropertyAsStringByKeyPrefix(final String keyPrefix,
      final String name, final String pomValue) {
    return getUserPropertyAsStringByKeyPrefix(keyPrefix, name, pomValue, null);
  }

  protected String getUserPropertyAsStringByKeyPrefix(final String keyPrefix,
      final String name, final String pomValue, final String defaultValue) {
    return getUserPropertyAsString(keyPrefix, name, pomValue, defaultValue);
  }

  protected String getUserPropertyAsString(final String name,
      final String pomValue) {
    return getUserPropertyAsString(name, pomValue, null);
  }

  protected String getUserPropertyAsString(final String name,
      final String pomValue, final String defaultValue) {
    return getUserPropertyAsString("apptools.", name, pomValue, defaultValue);
  }

  protected String getUserPropertyAsString(final String keyPrefix,
      final String name, final String pomValue, final String defaultValue) {
    final UserPropertiesManager manager = getUserPropertiesManager();
    String value = manager.getProperty(keyPrefix + name);
    if (StringUtils.isBlank(value)) {
      value = manager.getProperty(name);
    }
    if (StringUtils.isBlank(value)) {
      value = pomValue;
    }
    if (StringUtils.isBlank(value)) {
      value = defaultValue;
    }

    return value;
  }

  protected Integer getUserPropertyAsInteger(final String name,
      final Integer pomValue, final Integer defaultValue) {
    return getUserPropertyAsInteger("apptools.", name, pomValue, defaultValue);
  }

  protected Integer getUserPropertyAsInteger(final String keyPrefix,
      final String name, final Integer pomValue, final Integer defaultValue) {
    final UserPropertiesManager manager = getUserPropertiesManager();
    Integer value = manager.getPropertyAsInteger(keyPrefix + name);
    if (null == value) {
      value = manager.getPropertyAsInteger(name);
    }
    if (null == value) {
      value = pomValue;
    }
    if (null == value) {
      value = defaultValue;
    }
    if (null == value) {
      value = 0;
    }

    return value;
  }

  protected Long getUserPropertyAsLong(final String name, final Long pomValue,
      final Long defaultValue) {
    return getUserPropertyAsLong("apptools.", name, pomValue, defaultValue);
  }

  protected Long getUserPropertyAsLong(final String keyPrefix,
      final String name, final Long pomValue, final Long defaultValue) {
    final UserPropertiesManager manager = getUserPropertiesManager();
    Long value = manager.getPropertyAsLong(keyPrefix + name);
    if (null == value) {
      value = manager.getPropertyAsLong(name);
    }
    if (null == value) {
      value = pomValue;
    }
    if (null == value) {
      value = defaultValue;
    }
    if (null == value) {
      value = 0L;
    }

    return value;
  }

  protected List<String> getUserPropertyAsList(final String name,
      final List<String> pomValue) {
    return getUserPropertyAsList(name, pomValue, new ArrayList<>());
  }

  protected List<String> getUserPropertyAsList(final String name,
      final List<String> pomValue, final List<String> defaultValue) {
    final UserPropertiesManager manager = getUserPropertiesManager();
    List<String> value = manager.getPropertyAsList("apptools." + name);
    if (value == null || value.isEmpty()) {
      value = manager.getPropertyAsList(name);
    }
    if (value == null || value.isEmpty()) {
      value = pomValue;
    }
    if (value == null || value.isEmpty()) {
      value = defaultValue;
    }
    if (value == null || value.isEmpty()) {
      value = new ArrayList<>();
    }

    return value;
  }

  protected boolean isSkip() {
    return isUserPropertySet("skip") || skip;
  }

  protected boolean isVerbose() {
    return isUserPropertySet("verbose") || verbose;
  }

  protected List<Profile> calcActiveEnvironmentProfiles() {
    final List<Profile> activeEnvProfiles = new ArrayList<>();
    for (final Profile profile : project.getActiveProfiles()) {
      final String id = profile.getId();
      if (StringUtilities.hasNoLowerCase(id)) {
        activeEnvProfiles.add(profile);
      }
    }
    return activeEnvProfiles;
  }

  private boolean isDryRun() {
    return isUserPropertySet("dryRun") || isDryRunSetOnTheMojo();
  }

  protected abstract boolean isDryRunSetOnTheMojo();

  // --- business -------------------------------------------------------------

  protected void init() throws AppException {}

  // --- object basics --------------------------------------------------------

}
