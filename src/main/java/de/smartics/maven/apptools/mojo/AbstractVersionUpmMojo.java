/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.apptools.mojo;

import de.smartics.maven.apptools.domain.AppException;
import de.smartics.maven.apptools.domain.PluginAcceptorContext;
import de.smartics.maven.apptools.resources.Plugin;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.path.PathTranslator;
import org.apache.maven.settings.Settings;
import org.codehaus.mojo.versions.api.DefaultVersionsHelper;

import java.util.List;

/**
 * Encapsulates the version calculation dependencies.
 */
@SuppressWarnings("deprecation")
public abstract class AbstractVersionUpmMojo extends AbstractUpmMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Component
  private org.apache.maven.artifact.factory.ArtifactFactory artifactFactory;

  @Component
  private org.apache.maven.artifact.resolver.ArtifactResolver resolver;

  @Component
  private MavenProjectBuilder projectBuilder;

  @Parameter(defaultValue = "${reactorProjects}", required = true,
      readonly = true)
  private List<MavenProject> reactorProjects;

  @Component
  private ArtifactMetadataSource artifactMetadataSource;

  @Parameter(defaultValue = "${project.remoteArtifactRepositories}",
      readonly = true)
  private List<ArtifactRepository> remoteArtifactRepositories;

  @Parameter(defaultValue = "${project.pluginArtifactRepositories}",
      readonly = true)
  private List<ArtifactRepository> remotePluginRepositories;

  @Parameter(defaultValue = "${localRepository}", readonly = true)
  private ArtifactRepository localRepository;

  @Component
  private WagonManager wagonManager;

  @Parameter(defaultValue = "${settings}", readonly = true)
  private Settings settings;

  /**
   * URI of a ruleSet file containing the rules that control how to compare
   * version numbers. The URI could be either a Wagon URI or a classpath URI
   * (e.g. <code>classpath:///package/sub/package/rules.xml</code>).
   *
   * @since 1.0
   */
  @Parameter(property = "rulesUri")
  private String rulesUri;

  @Component
  private PathTranslator pathTranslator;

  @Component
  private ArtifactResolver artifactResolver;

  @Component
  protected ArtifactHandler artifactHandler;

  /**
   * The server ID to lookup additional credentials in the <tt>settings.xml<tt>.
   * This is used when wagon needs extra authentication information.
   * <p>
   * User property: <tt>apptools.artifactServerId</tt>,
   * <tt>artifactServerId</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String artifactServerId;

  /**
   * If set to <code>true</code> only releases are considered. Otherwise
   * snapshots are also taken into account.
   * <p>
   * User property: <tt>apptools.requireReleases</tt>, <tt>requireReleases</tt>
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "false")
  private boolean requireReleases;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public abstract class VersionUpmContext extends BaseUpmContext
      implements PluginAcceptorContext {
    private final DefaultVersionsHelper versionsHelper;

    protected VersionUpmContext() throws AppException {
      try {
        versionsHelper = new DefaultVersionsHelper(artifactFactory,
            artifactResolver, artifactMetadataSource,
            remoteArtifactRepositories, remotePluginRepositories,
            localRepository, wagonManager, settings, getArtifactServerId(),
            rulesUri, getLog(), mavenSession, pathTranslator);
      } catch (final MojoExecutionException e) {
        throw new AppException("Cannot create versions environment.", e);
      }
    }

    public DefaultVersionsHelper getVersionsHelper() {
      return versionsHelper;
    }

    public boolean isRequireReleases() {
      return AbstractVersionUpmMojo.this.isRequireReleases();
    }


    public Artifact createArtifact(final String key, final String version,
        final String type) {
      final int index = key.lastIndexOf('.');
      final Artifact artifact =
          new DefaultArtifact(key.substring(0, index), key.substring(index + 1),
              version, "compile", type, null, artifactHandler);
      return artifact;
    }

    @Override
    public abstract boolean isAccepted(Plugin plugin);
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getArtifactServerId() {
    return getUserPropertyAsString("artifactServerId", artifactServerId);
  }

  public boolean isRequireReleases() {
    return isUserPropertySet("requireReleases") || requireReleases;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
